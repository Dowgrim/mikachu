export default {
  en: {
    navbar: {
      home: "Home",
      devs: "Developments",
      travels: "Travels",
      about: "About",
      "travel-eu": "Around Europe",
      "travel-jap": "Japan",
      "travel-mar": "Moroco",
      "travel-sco": "Scotland",
      "travel-nz": "New Zealand",
      "travel-dub": "Dublin",
      "travel-zur": "Zurich",
      "travel-ams": "Amsterdam",
      "dev-dem": "Minesweeper",
      "dev-sna": "Snake",
      "dev-sdem": "Deminator",
      "dev-mer": "Merchant",
    },
    travels: {
      nz: {
        home: {
          title: "Mon aventure en Nouvelle-Zélande",
          "intro-text":
            "En septembre 2019 je suis parti en Nouvelle Zélande pour une année. C'est une aventure assez longue, plutôt que de faire une page à rallonge, je vais la séparer par semaine ou par mois en fonction des choses à raconter. Sachant que pendant ce voyage j'ai tenu un groupe Facebook ou je racontais ce que je faisais, vous allez voir ici une version plus propre et imagé. \nMais avant toute chose je vais commencer par répondre aux question posées régulièrement les fameuses : \"Qu'est-ce qui t'as amené à faire ça ?\", \"Pourquoi la Nouvelle-Zélande ?\", \"Qu'est-ce que tu vas faire là bas ?\" et sûrement d'autre dans une petite ...",
          "intro-link": "Introduction à mon aventure à l'autre bout du monde!",
          "first-month-link": "L'arrivé et le premier mois à Auckland",
        },
        intro: {
          title: "Before departure",
        },
        "first-week": {
          title: "First week by bicycle",
        },
      },
    },
  },
  fr: {
    navbar: {
      home: "Accueil",
      travels: "Voyages",
      about: "À propos",
      "travel-jap": "Japon",
      "travel-mar": "Maroc",
      "travel-sco": "Ecosse",
      "travel-nz": "Nouvelle Zélande",
      "travel-dub": "Dublin",
      "travel-zur": "Zurich",
      "travel-ams": "Amsterdam",
      devs: "Développements",
      "dev-sporz": "Sporz",
      "dev-dem": "Démineur",
      "dev-sna": "Snake",
      "dev-sdem": "Déminator",
      "dev-mer": "Marchant",
    },
    home: {
      welcome: "Hey, bienvenue sur mon site !",
      why: "J'ai envie de partager, c'est ici que je le fais.",
      wish: "Bonne visite!",
    },
    about: {
      nav: {
        site: "Mon site web",
        me: "Moi",
        recommendation: "Mes recommendations",
        contact: "Me contacter",
      },
      "web-site":
        "J'ai développé ce site pour en faire un blog de mon aventure en Nouvelle-Zélande. Faire ça m'a donné envie de ne pas m'arrêter à simplement un blog et de continuer avec d'autre voyage et d'autres projets.\n\
            ",
      "about-me": "",
      recommendation:
        "Ne m'écoute pas, pense par toi même.\n\n\n Réfléchi à ce que tu viens de lire.",
      contacts: "Todo",
    },
    travels: {
      moroco: {
        home: {
          pre: {
            title: "Partir seul au Maroc",
            name: "maroc",
            "date-inter": "27/03/2019 - 15/04/2019",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "next-link": "beforeNZ",
          },
        },
        intro: {

        },
        "01-Marakech":{
          pre:{
            title:"Arrivé à Marakech et départ vers le désert",
            name:"marakech",
            path:"/travel/moroco/marakech",
            "date-inter": "27/03/2019 - 31/03/2019",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "introMoroco",
            "next-link": "??",
          },
          data:{
            "27/03/2019" : {
              text: 
"J'atterie donc à Marakech. Dans l'aéroport on me propose une carte sim orange, incroyable comme c'est pas cher ici ( 10€ = 10go... Comparé à la Nouvelle-Zélande 12€ = 1.25go...). Je prends le bus pour le centre, je me balade un peu sur la place Jemaa el-Fna et dans les environs avant de me diriger vers l'auberge que j'ai réservé pour 4€ la nuit avec petit dej... C'est une première fois pour moi dans un pays ou la vie est peu cher, ça m'a impressionné tout du long. \n\
C'est la fin d'après midi donc je traine un peu en organisant mes prochains jours.\n\
En début de soirée je discute avec une Française et une Allemande qui m'invitent à vadrouiller en ville avec elles, je les accompagne et on mange dans un petit restaurant qui ne paye pas de mine mais c'était super bon ! Elles m'invitent et me disent : \"tu inviteras quelqu'un toi aussi et tu lui diras de faire de même\", j'ai adoré l'initiative et j'essaye de refaire ça de temps en temps =) !\n\
On continue à se perdre dans la medina jusqu'à très tard où quasiment plus personne n'y marche, sauf quelques personnes qui vivent la nuit. On croise un marocain qui travaillait encore, et on marche avec lui quelques minutes en discutant quand il nous dit qu'il a un dernier truc à faire avant de finir son taff et qu'il va finir la soirée dans un bar à chicha, il nous indique la direction et on va finir la soirée la bas.",
            },
            "28/03/2019": {
              text: 
"On s'est pas couché tôt hier soir mais les filles sont déjà partie à mon réveil. Je réserve une autre nuit à l'auberge puisque je ne sais pas encore ce que je vais faire de mes jours suivant.\n\
Je vais me balader en ville en millieu de journée, je manque dans un snack en périférie du centre ville, je fais un musée et je tourne tout autour de la Médina, c'est vraiment pas la même ville de jour. Je retourne à l'auberge en fin d'aprés midi et je me couche assez tôt pour récupérer de la veille et être prés pour le réveil du lendemain."
            },
            "29/03/2019":{
              text:
"Réveil aux aurores pour prendre le bus vers Zagora. C'est un tour organisé avec un tour en chameau, une nuit \"traditionnel\" (ça peut pas vraiment être traditionnel dans ce genre de conditions) et le retour à Marrakech, pour 55€, je trouvais que ça valait le coup :). On prend un minibuss depuis le centre ville vers les montagnes. Passer dans sur cette petite route de l'Atlas offrent des paysages vraiment sympathique! Sur le trajet on s'arrête à Ait BenHaddou qui est un bled qui ne fonctionne que par le tourisme et qui a permis de tourner de nombreux plan de film apparemment. On en fait le tour et des gens y habitent encore. Vu le nombre de bus de touriste passant par là, ça doit pas être trés agréable mais ils doivent bien gagner leur vie au moins! On mange sur place dans un resto hotel hors de prix et non compris... Pas très étonnant mais c'était bon :).\n\
On arrive en fin d'après midi à Zagora, la route a été longue ! On nous dépose un peu plus loin où des chameaux nous attendent pour nous amener jusqu'au campement. On a le droit à un superbe coucher de soleil à dos de Chameau, je suis satisfait de cette expérience =).\n\
La soirée est très sympathique, on a très bien mangé, on a beaucoup bu de thé hyper sucré et on a eu droit à pas mal d'animation musicale de la part des responsable du campement mais aussi de la part des autres invités qui se sont pris au jeu =)."
            },
            "30/03/2019": {
              text:
"Personnellement j'ai bien dormis dans cette petite tente. Peu traditionnel vu sa solidité mais qui a quand même son côté atypique. La température est bien descendu pendant la nuit, heureusement qu'on avait beaucoup de couverture.\n\
On repart en chameau jusqu'au bus.\n\
Je demande à ce qu'on me dépose à Zagora pour profiter encore un peu de l'endroit et j'en profite pour réservé un hotel dans la ville. \n\
Je passe à la gare de bus pour réservé un ticket pour le lendemain matin, on me dit que ce n'est pas la peine, qu'il y aura de la place sans problème (spoiler : y'avait plus de place...), je vais déposer mes affaires à l'auberge, c'est un peu tôt mais ils acceptent de garder mon sac. Je m'en vais donc à la recherche d'un moyen de louer un quad ou une moto pour en faire dans le désert sur le sable ! J'ai fait ça en Egypte quand j'étais très jeune avec mes parents et j'en garde un souvenir incroyable. Je cherche sur le net sans grand succès mais finalement j'apprends qu'il faut que j'aille à Mhamid, qui est le dernier village avant le désert. Je vais donc prendre un taxi pour la bas.\n\
Arrivé sur place on m'accoste trés rapidement pour \"m'aider\". Je me laisse amadouer en restant sur mes gardes mais le gars est sympas et finit par me vendre un petit aller retour dans le désert avec le repas du midi inclu... Un prix exorbitant je trouve.... mais bon tant pis y faut profiter et le repas était vraiment hyper bon !\n\
J'ai l'occasion de marcher seul dans le désert, c'est vraiment quelque chose... et encore c'est des conditions où la civilisation n'est seulement qu'a 1 ou 2 kilomètres et où je captais encore internet, il y a pas mal d'expedition de marche sur plusieurs jours où en 4x4 pour aller jusqu'à des oasis.\n\
Pour repartir je prends un minibus, c'est moins cher mais on est séré !\n\
Une bonne nuit à l'hotel ça fait pas de mal !"
            },
            "31/03/2019":{
              text:
"Le lendemain encore un réveil tôt pour aller prendre le bus et comme spoilé précédemment, il n'y a plus de place alors que j'arrive 20 mins avant le départ... J'ai plus envie de dépenser trop d'argent donc je décide de marcher aujourd'hui, je vois une colline au loin, j'en fais ma destination. C'était pas si loin, environ 5kms de marche sur le bord de la route. Il est seulement 9h et le prochain bus est à 18h... J'ai pris un livre, mais après 3h à lire, je commence à saturé et en plus de ça il se met à pleuvoir ... Je me mets sous ma cape de pluie pour attendre que ça passe. Je fais une petite sieste, le temps est assez long mais l'heure arrive finalement ! Evidemment j'avais réservé le billet le matin et c'est reparti pour Marrakech."
            }
          }
        }
      },
      scotland: {},
      nz: {
        home: {
          pre: {
            title: "Mon aventure en Nouvelle-Zélande",
            name: "newZealand",
            "date-inter": "12/09/2019 - 07/07/2020",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "next-link": "beforeNZ",
          },
          "intro-ad": "En cours de finition",
          "intro-text":
            "En septembre 2019 je suis parti en Nouvelle Zélande pour une année.C'est une aventure assez longue, plutôt que de faire une page à rallonge, je vais la séparer par semaine ou par mois en fonction des choses à raconter. Sachant que pendant ce voyage j'ai tenu un groupe Facebook où je racontais ce que je faisais, j'ai envie de retranscrire ça presque à l'identique en y ajoutant des précisions et des images.\n\
Mais avant toute chose je vais commencer par répondre aux questions posées régulièrement les fameuses: \"Qu'est-ce qui t'as amené à faire ça ?\", \"Pourquoi la Nouvelle-Zélande ?\", \"Qu'est-ce que tu voulais faire là bas ?\" et sûrement d'autre dans une petite ...",
          "intro-link": "Introduction à mon aventure à l'autre bout du monde!",
          "first-month-link": "L'arrivé et le premier mois à Auckland",
        },
        intro: {
          pre: {
            title: "Introduction à mon aventure à l'autre bout du monde",
            name: "beforeNZ",
            path: "/travels/new-zealand/introduction",
            "date-inter": "... - 10/09/2019",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "newZealand",
            "next-link": "nz1",
          },
          how:
            "Depuis la fin de mes études j'avais pour projet d'aller passer quelques temps à l'étranger. J'ai finalement décidé de m'installer quelques années dans un pays anglophone pour perfectionner mon anglais (j'en avais bien besoin avant de m'attaquer à une langue de plus) et expérimenter la vie dans un autre pays. J'ai choisi la Nouvelle Zélande, j'avais envie de m'éloigner de l'europe, j'hésitais entre le Canada, l'Australie ou la Nouvelle-Zélande. Après quelques temps j'ai choisie le coin Australie/Nouvelle-Zélande par préférence et quand j'en parlais autour de moi tout le monde avait la Nouvelle-Zélande en très haute estime, j'ai donc écouter leurs avis!",
          todo:
            "Je vais maintenant vous parler de ce que j'ai préparer pour partir et même ce que j'aurai dû préparer.",
          "visa-title": "Le VISA",
          "visa-text":
            "Pour rentrer dans le pays vous pouvez, en tant que Français, utilisait un VISA Touriste, ça vaut pour énormément de pays dans le monde. Mais pour rester dans le pays plus de 3 mois, il faut faire une demande de VISA, plusieurs solutions sont possible :\n\
                    - Un VISA Touriste plus long. Apparement les démarches sont compliquées.\n\
                    - Un VISA Travail, qui nécessite d'être sponsorisé par un employeur. Et que l'employeur est une bonne raison. J'espérais être sponsorisé après avoir trouvé un taff.\n \
                    - Un VISA Résident, qui peut se demander en ayant une bonne situation(métier demandé, bon diplôme, expérience dans le domaine...). J'étais proche de pouvoir faire la demande mais y me manquait quand même quelques points. \n\
                    - Un VISA Vacance-Travail, ou Permis Vacance Travail(PVT). C'est la solution que j'ai utilisé. Ce VISA permet de rester 1 an dans le pays et de pouvoir y travailler. \n \
                    Le PVT, était vraiment facile à obtenir, il n'y avait pas de limite donc il suffit de s'inscrire, d'envoyer quelques papiers et de payer 300€ pour l'avoir. Le seul petit soucis de passer par cette intermédiaire est qu'il faut rentrer dans le pays pour commencer un VISA, donc pour passer à un VISA travail il aurait fallu que je fasse un aller-retour avec l'Australie.",
          "money-title": "L'argent",
          "money-text":
            "Super important d'utiliser Revolut, N26 ou tout autre banque spécialisé dans l'échange de monnaies. Pour avoir fait des tests, il y a souvent environ 10% d'écart avec le taux de change que propose votre banque. Faites les retraits en liquide dans un distributeur WestPac pour éviter des frais dû à votre carte étrangère.\
                    Pour travailler en Nouvelle-Zélande il vous faudra une compte bancaire Néo-zélandais. Les seules banque acceptant les étrangers sont Kiwi-bank et ANZ. Peu de différence entre les deux d'après ce que j'ai compris. La demande de rendez-vous peut se faire en ligne ou en direct.\
                    Enfin vous allez avoir besoin d'un IRD number(Inland Revenue Department). La demande s'effectue en ligne après avoir obtenue votre compte bancaire.",
          "taff-title": "Le travail en Nouvelle-Zélande",
          "taff-text":
            "Il faut savoir qu'il y a énormément de PVT en Nouvelle-Zélande, la concurrence est rude, et la majeur partie des boulots vont uniquement se fier à vos expériences professionnels pour faire leur choix. En plus de ça, ils demandent fréquemment des références, c'est important d'en avoir au moins 1 avant de partir. \n\
Les sites important: Trade - me, seek, backpackerboard et Facebook.",
          "conclusion":
            "Finalement, pas tant de chose à préparer ! Ne vous inquiètez pas trop et lancez vous ! =)\n\n\
Juste avant le départ j'ai pas mal douté, et prendre l'avion n'a pas été si facile. \n\
Voilà ce que j'ai écris :",
          "fb":
            "Ca y est c'est le grand départ!\n\
Depuis quelques jours je me pose plein de questions sur mon choix et mes objectifs que je trouve si ambitieux et quand même complètement illogiques... Je quitte une région magnifique, ma famille, des amis en or, et tout ça pour partir dans un pays inconnu où il fait froid et où je ne connais personne... faut croire que je suis fou ^^ ! Mais c'est plus du tout le moment de faire demi-tour et puis je me dirai plus ca une fois sur les pistes, alors c'est parti ! Hier c'était journée rangement, toute ma vie rentre dans 10 cartons, 1 valise et 1 sac même si pour ca j'ai dú en jeter pas mal ! Si un jour vous vous ennuyez, franchement c'était vraiment une bonne journée pleine de souvenir. Je pars donc avec une valise de 25kg et un sac à dos de 13kg. Et avec ca j'ai vraiment tout ce qu'il faut pour vivre : mon ordi, ma combinaison de ski, mon matos de camping, mon matos d'escalade, un jeu de société et puis quelques habits ;).\n\
                    \n\
Premier vol pour Istanbul de 3h.\n\
Deuxième vol pour Bangkok de 9h...\n\
Dernier vol pour Auckland de 11h.............\n\
\n\
Et j'arrive jeudi à 0h45 (10h45 heure local)_n\
\n\
Passer une super journée de mercredi pour compenser la mienne !\n\
\n\
A bientôt !",
          "retro":
          "Je vais vous faire une petite retrospective concernant",
        },
        "01-first-month": {
          pre: {
            title: "L'arrivé et le premier mois à Auckland",
            name: "nz1",
            path: "/travels/new-zealand/first-month",
            "date-inter": "12/09/2019 - 18/10/2019",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "beforeNZ",
            "next-link": "nz2",
          },
          data: {
            "12/09/2019": {
              text:
                "Arrivé à Auckland ! C'est vachement long 32h de trajet mais c'est assez vite passé et pour le moment je suis pas trop décalqué.\n\
\
Après avoir fait un petit tour de la ville, j'aime bien la météo, et c'est vrai que les gens sont vachement accueillant:).\n\
\
J'ai commencé par faire les demarches pour ouvrir un compte bancaire et on enchaine direct sur de la recherche de boulot.\n\
Finalement j'abandonne le ski... 100€ par jour de forfait + 30€ de location + 60€ de logement(30€ l'auberge et 30€ de bus) pour le moins cher... Et accessoirement y'a 6h de bus.On verra ça quand j'aurai quitté mon statut chômeur ;) ^^. ",
            },
            "15/09/2019": {
              text:
                "Ca y est le jetlag s'estompe petit à petit ! Même si ça m'a pas empeché de me réveiller à 6h du mat après m'être couché à minuit.... :'(\n\
\
Les 3 dernier jours j'ai beaucoup marché dans la ville, fait un peu d'escalade, manger avec quelqu'un de ma chambre d'auberge, passer la soirée d'hier chez un contact qu'on m'avait filé (très bonne soirée) et passer tout plein de temps sur le pc pour chercher un boulot et une colloc.\n\
Rien de très passionnant à partager pour le moment.",
            },
            "17/09/2019": {
              text:
                "Toujours à la recherche d'une colloc et d'un taff sympas mais pour commencer on m'a proposé un boulot à l'occasion des élections qui auront lieu dans 3 semaines. Je vais dépouiller des lettres, ça permettra d'avoir une première expérience et de faire d’autres rencontres :).\n\
\
Globalement je passe mes journées à vadrouiller dans la ville avec différentes rencontre de l’auberge. Y’a plein de parc super cool(d’ancien volcan en fait ^^) tous différents les uns des autres et à côté de chaque parc y’a des jeux pour enfants… ici c’est incroyable !! Des tyrolienne, des trains, des balançoires super originales, des toboggans de 15m! Je vous prie de ne pas juger, mais il fallait évidemment essayer tout ça!\n\
\
Hier j’ai pris le Ferry pour aller explorer une île en face d'Auckland, Rangitoto island ! C'est une île volcanique et la bas j’ai appris que c'est blindé de volcans Auckland… apparemment endormi mais pas forcément éteint...",
            },
            "27/09/2019": {
              text:
                "après 1 semaine et demi ca y est le décalage horaire s'est estompé complètement, enfin possible de profiter des soirées ! Et justement samedi soir c'était les premiers matches de la world cup de rugby 🏉🏉 ! Evidemment vous connaissez ma passion pour ce sport donc j'ai invité les quelques personne avec qui j'ai sympathisé pour aller voir les matchs dans un bar ! Plus sincèrement, j'ai organisé la soirée et l'un d'eux m'a dit qu'il y avait match ! ^^ On s'inquiétait de ne pas trouver de place puisqu'on est quand même dans le pays du rugby !!! Et ben grosse idée reçu, c'était complètement mort...Pour un samedi soir avec match, on avait le bar presque pour nous jusqu'à l'heure du match et à ce moment là on pouvait encore compter des tables vides. \
Mais en tout cas très bonne soirée même je vais vous décevoir, y'a que des Français ici ! Je parle quasiment jamais Anglais ^^.\n\
\
Sinon toujours à l'auberge de jeunesse, la ville est surchargé... C'est très difficile de trouver une coloc sans boulot. Mais bon je suis pas difficile et l'auberge est pas cher. Même si je ne peux nier que les gens s'étalent beaucoup trop dans le dortoir... Et la cuisine est pourris, pas sale mais vraiment usé, des plaques de cuisson vraiment lente et des poêles datant de l'avant guerre... Pas vraiment de quoi ce cuisiner de bon repas.\
Une amie d'ici à emménagé dans un genre d'hôtel, c'est vraiment pas mal mais c'est 2 fois plus cher, et presque tout le reste demande à rester des mois...pour le moment je vais jouer les économies.\n\
\
Très compliqué de trouver un boulot dans l'info! Je pense qu'il faut que je mette plus en avant mes plans de m'installer. Mon PVT ne dois pas trop les attirer. J'ai pour l'instant eu aucun entretien :/.\n\
\
En ce moment j'essaye d'obtenir le numéro d'identification national qui me permettra de ne pas être trop taxé sur le salaire en gros. C'est le bordel ! Mon Visa avait un soucis mais même maintenant résolu y'a toujours des complications.",
            },
            "18/10/2019": {
              text:
                "Après seulement 1 mois... Y'a un petit changement de plan... Plutôt que de m'installer à Auckland, je pars faire un tour de la Nouvelle-Zélande en vélo avec une fille que j'ai rencontré ici ! ^^\n\
\
Elle vient d'Antibes... Elle est partie la veille de mon départ et on était dans le même dortoir le jour de mon arrivé et depuis on s'est pas trop laché.\n\
\
Et le boulot, on l'a fait ensemble parce que c'est elle qui me l'a proposé, le hasard a fait qu'on était à côté...Et on a finalement pris une chambre de coloc ensemble. Enfin bon ca se passe bien ! \n\
\
Le boulot n'était pas si ennuyeux que ça, on était avec des gens vraiment super sympas! Ca a rendu le travail beaucoup plus divertissant. Et ca m'a fait pratiquer l'anglais, c'était vraiment super !\n\
\
Depuis cette grosse décision j'ai acheté un vélo et de quoi l'équiper pour le voyage.\n\
\
On est parti ce matin ! Notre premier objectif est d'aller faire la saison des fruits sur l'île du nord, ca devrait nous prendre 3 semaines d'atteindre l'endroit (au alentour de la ville de Napier) et on verra une fois sur place =).\n\
Enfin ça c'était le plan d'origine.\n",
            },
          },
        },
        "02-bike-start": {
          pre: {
            title: "Première semaine sur le vélo",
            path: "/travels/new-zealand/bike-start",
            name: "nz2",
            "date-inter": "18/10/2019 - 24/09/2019",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "nz1",
            "next-link": "nz3",
          },
          data: {
            "18/10/2019": {
              km: "45",
              text:
                "Départ d'Auckland ! après avoir rempli les sacoches et accrochés les sacs à dos, on est parti pour 700m de dénivelé et 45km...\n\
Avant d'arriver au début de la montée, on traverse la ville tranquillement et en regardant mon téléphone, je me prends une barrière pour commencer ce voyage avec une chute ^^. Rien de grave bien sûr:).\n\
Je vais pas vous cacher que cette première ascension fut très compliquée ! après 1/3, je me disais que je tiendrais pas jusqu'en haut mais finalement ça s'est fait ! Et pour se récompenser d'arriver en haut, petit resto avec une superbe vue sur Auckland et ses environs.\n\
La redescente se fait attendre par un enchaînement de valon pour finalement arriver sur Piha !\n\
C'est incroyablement beau ! Seul bémol, c'est complètement interdit de faire du camping sauvage sous peine de 120€ d'amende chacun... Donc petit camping à 10€ chacun, au moins on a une douche :).",
            },
            "19/10/2019": {
              km: "0",
              text:
                "Y fait pas super beau, y'a eu beaucoup de vent pendant la nuit et ça continue. On est vraiment cassé de ce premier jour et je ne peux pas reposer le cul sur la selle sans douleur, et ça tombe bien parce qu'on avait prévu de rester une journée ici! \n\
Y fait assez froid mais on prend le temps d'aller marcher sur la plage, un gars en char à voile s'éclate, c'est vrai qu'avec cette tempête ça doit être sympas !\n\
Le temps s'améliore dans l'après midi, parfait pour visiter un peu les environs en grimpant sur le gros rocher et en empruntant un chemin faisant le tour des environs par les hauteurs.",
            },
            "20/10/2019": {
              km: "42",
              text:
                "Une nuit bien plus calme, je suis d'attaque pour repartir !\n\
En arrivant au camping, on nous avait promis un petit tour en van pour éviter la monter mais finalement la gentille madame nous ayant fait cette proposition n'est pas à l'accueil donc on y va! Et c'est tellement dure au début, mais petit à petit je prends le rythme et on atteint le restaurant bien plus vite que ce à quoi je m'attendais. C'est hyper satisfaisant d'y arriver comme ça !\n\
Une descente hyper plaisante et quelques pistes cyclables, nous voilà de nouveau à Auckland pour une nuit.",
            },
            "21/10/2019": {
              km: "45",
              text:
                "Deuxième départ ! Par bateau cette fois pour aller vers Beachlands ou Pine Harbours. Un petit cafouillage et une mauvaise information de la part d'une guichetière nous fait partir à 11h50... 1h30 à attendre sur un banc n'était pas ce qu'on avait prévu ^^. Finalement on a de la chance, on se prend la pluie juste pendant la traversé !\n\
C'est là que le voyage commence vraiment ! Une route magnifique, un temps magnifique et les km s'enchainent bien!\n\
On avait repéré un camping(Tawhitokino Campground) bien placé et pas cher mais on ne sait pas trop comment y aller, il est loin des routes donc on essaye de faire au plus cours.C'est un peu raté, seul moyen d'y aller c'est par la plage et en vélo, c'est impossible ! Mais finalement un habitant du coin nous propose de laisser nos vélos devant chez lui et de partir en sac à dos. On continue donc à pied sur une plage suivi de 500 marches dans un sens et dans l'autre pour arriver sur une plage gigantesque rien que pour nous ! Il fait toujours un peu trop froid pour aller à l'eau malheureusement.\n\
Le camping est complètement abandonné et pas du tout entretenu, on décide de ne pas payer les 10€ qu'ils demandent de déposer dans une boite à côté d'un tas de déchet...",
            },
            "22/10/2019": {
              km: "35",
              text:
                "L'endroit est toujours aussi magnifique à notre réveil mais il est temps de repartir ! À peine tout rangé qu'il commence à pleuvoir, heureusement pas très longtemps. La randonnée au réveil avec les jambes déjà cassées par le vélo, ça m'a pas mal handicapé pour le reste de la journée.\n\
Et ça continue mal ce matin, on a un vent de face horrible... On avance vraiment pas alors qu'on a prévu de faire une très grosse journée !\n\
Au bout de 30 km, on est complètement mort tous les deux (surtout moi), le vent ne veut vraiment pas nous laisser avancer, il est déjà 14h, on choisi le plan B. On continue jusqu'à un camping gratuit à Kaiaua, et on s'arrête là pour la nuit. Le soir je repère un genre de snack qui semble vraiment pas mal, on décide d'y manger. Vraiment pas de regret, pas cher et super bon !",
            },
            "23/10/2019": {
              km: "32",
              text:
                "Pluie battante et vent violent toute la nuit. A notre réveil il pleut encore, même si c'est pas en continue, les acalmis ne durent pas plus de 15mins. On voit du soleil, on se dit que c'est le moment et une fois sur les vélos, ça recommence. On se fait le petit dej dans le resto d'hier. Toujours très convaincu par ce petit snack.\n\
Une nouvelle acalmi nous décide à partir...après 5 mins à lutter contre le vent, la pluie recommence. La pluie, le vent et le froid... Vraiment pas un bon combo. C'était vraiment pas facile pendant 20 mins mais après ça s'est arrêté pour de bon, et on à eu du soleil ! Même si le vent, lui, continuait à nous fatiguer... Malgrés ça, c'était une agréable journée à rouler avec un dénivelé raisonnable :).\n\
Par contre la fatigue se fait sentir et on a besoin d'une douche ! On se pose dans une auberge de jeunesse dans les environs de Mercer, qui est en plus à côté d'un centre de parachute, je vais peut être pouvoir sauter !",
            },
            "24/10/2019": {
              km: "0",
              text:
                "On a rien fait, et ça fait du bien. Mauvaise météo, impossible de sauter en parachute et trop loin de quoi que ce soit pour faire un tour.",
            },
          },
        },
        "03-tauranga": {
          pre: {
            title: "En route pour Tauranga",
            path: "/travels/new-zealand/tauranga",
            name: "nz3",
            "date-inter": "25/10/2019 - 31/10/2019",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "nz2",
            "next-link": "nz4",
          },
          data: {
            "25/10/2019": {
              km: "35",
              text:
                "C'est le départ sans avoir pu faire de parachute ☹.\n\
Pas une bonne journée à l'horizon, beaucoup de vent... Et évidemment il est contre nous.\n\
après 5km je bloque ma chaine sous le dérailleur avant... Impossible de la décoincer, obligé de démonter le dérailleur. Facile à démonter mais pour le remonter correctement, vraiment une galère ! après une bonne demi-heure de tentative infructueuses c'est à peu près correct, c'est reparti pour du vent de face et du dénivelé...\n\
On arrive dans le village Mercer complètement démotivé. Mais il y a une fromagerie ici et le fromage est divin! On décide de manger sur place parce que Camille est pas rassurée de prendre une genre d'autoroute avec les vélos et c'est la seules solution raisonnable puisque l'autre solution est un détour de 25 km. Et on décide de racheter du fromage ^^.Finalement on a vraiment de la place de rouler loin des voitures et ça ne dure pas longtemps 🙂.\n\
L'aprem est aussi difficile que la matiné sans les galères techniques.\n\
Petit camping tranquil à Te Kauwhata, quasiment que pour nous et pas très cher.",
            },
            "26/10/2019": {
              km: "70",
              text:
                "Une nuit sans vent et avec très peu de pluie, enfin !!! Et au réveil, toujours pas de vent même si c'était totalement couvert. Ça y est c'est le moment de bien rouler !\n\
Et on a bien roulé... On a fait les 70km d'aujourd'hui sans se poser plus de 5 mins... Y'avait rien pour s'arrêter de mieux que l'entrée de chemin privée... On a cherché pourtant mais rien à faire.\n\
Arrivé à 15h à Paeroa avec le soleil et des avocats pas cher et super bon!\n\
Petit camping sympas dans lequel on rencontre Victor un Français cyclotouriste qui vient de finir son PVT en Australie et veut continuer quelques mois avant de revenir en France(si ce genre d'histoire vous plaît aller voir sa page FB, Vico & Louping ).",
            },
            "27/10/2019": {
              km: "35",
              text:
                "Au réveil, grand soleil et pas un souffle, une super journée en perspective. On quitte Victor, qui descend vers le sud, pour nous aller vers l'est, depart compliqué mon dérailleur avant fait encore des siennes... Tant pis on va faire avec!\n\
Aujourd'hui un petit chemin qui traverse une vallée au bord d'une rivière, c'était vraiment trop bien ! En plus le dénivelé était étalé sur toute la durée de la montée, c'était que du plaisir.\n\
Sur la fin une petite descente vers la mer, vraiment grandiose pendant quelques secondes (j'ai tenté de prendre la photo sur le vélo mais un peu trop tard).\n\
Ce soir c'est camping avec source thermal, dans le petit village d'Athenree.",
            },
            "28/10/2019": {
              km: "0",
              text:
                "On a bien aimé le camping et le cadre est génial, on y prend une seconde nuit.\n\
\
Je me motive à aller jusqu'au patelin voisin en vélo pour faire 3 courses et profiter un peu de la belle plage.\n\
\
Une grosse discussion occupe une bonne partie de l'après midi. Camille ne veut plus faire du vélo et va partir à pied faire le Te Araroa, une randonnée qui traverse la Nouvelle Zélande du nord au sud. Moi je vais continuer en vélo ! Mais on reste \"ensemble\" et on se retrouvera vers Wellington pour Noël :).\n\
On va donc passer les quelques prochains jour ensemble avant de partir chacun de son côté.",
            },
            "29/10/2019": {
              km: "60",
              text:
                "Réveil trop tardif pour profiter de la piscine, tant pis, y'aura d'autre occasion ! Et l'objectif d'aujourd'hui c'est Tauranga, une des plus grosse ville du pays avec ces 130 000 habitants ^^.\n\
\
On roule bien mais on est sur \"l'express way 2\", une genre d'autoroute.\n\
\
Pour s'arrêter à midi on se fait un petit détour vers le bord de mer, mais le seul endroit correct est à côté d'un chantier...\n\
\
Pendant cette pause Camille reçoit la réponse d'une famille à qui elle avait demandé de déposer son vélo pour quelques mois. Judy (notre interlocutrice) nous propose de loger dans un petit chalet derrière chez eux pour moins cher que ce qu'on pourrait trouver ailleurs dans la ville.Et elle nous invite a manger pour le soir si on y participe un peu, on décide de faire des crêpes !\n\
\
C'est bien beau toute cette organisation mais il nous reste 30 km ! Et je vous rappel que ce n'est jamais plat en NZ ^^.\n\
\
On est fatigué et lors d'une nouvelle pause on se dit qu'on devrait travailler à Tauranga quelques temps avant de se quitter !\n\
\
Arrivé chez Judy, le chalet est vraiment top ! Le repas se passe super bien, et c'est super bon !\n\
\
On comptait ne rester qu'une nuit mais finalement elle nous propose de rester quelques temps et notre plan de dernière minute nous impose de rester dans la ville quelques jours.",
            },
            "30, 31/10/2019": {
              km: "0",
              text:
                "Aujourd'hui on cherche un boulot ! Et c'est pas facile...La plupart des boulots sont des contrats de 3, 4, 6 mois, alors qu'on cherche plutôt sur quelques semaines.\n\
\
Finalement en fin de journée, on trouve une super offre pour un champ qui à l'air très sympathique. On postule et on nous dit de nous rendre le 1er novembre à Te puke, pour signer pour le job !\n\
\
Sinon, Judy habite dans la ville à côté de Tauranga, qui s'appel Mont Manganoui à cause d'un mont volcanique qui surplombe la ville.C'est vraiment une ville où il fait bon vivre, la plage est gigantesque et magnifique, il n'y a que des maisons, aucun immeuble à part le centre commercial qui contient tout ce dont on peut avoir besoin: supermarché, magasin spécialisé en ce que vous voulez, restaurant...D'ailleurs un truc pas mal en NZ c'est le prix des pizzas...Domino's pizza ou pizza hut vendent des pizzas à 3, 4€ qu'on trouverait à 10, 12 en France. On en profite régulièrement :/ ^^.\n\
\
On décide de faire à manger à nos hôtes un soir, ils acceptent et on se retrouve au fourneaux à cuisiner pour 8! Au menu, une quiche courgette- feta, une ratatouille et du riz. (J'ai fait le comi de cuisine, pas vraiment des plats que j'ai l'habitude de faire) Le seul soucis c'était de trouver une pâte feuilletée, impossible ici...Alors on a fait ca avec de la pâte à pizza tout faîte.On avait clairement pas le temps de la faire nous.\n\
Le repas a été très apprécié et nous a permis de connaître un peu mieux le père, David.",
            },
          },
        },
        "04-kiwi-picking": {
          pre: {
            title: "Prêt à travailler ! ",
            path: "/travels/new-zealand/kiwi-picking",
            name: "nz4",
            "date-inter": "01/11/2019 - 09/11/2019",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "nz3",
            "next-link": "nz5",
          },
          data: {
            "01/11/2019": {
              km: "35",
              text:
                "Le matin on traine un peu, on a rendez - vous à 16h à Te puke.Départ vers 11h, le gps nous dit de passer par la voie express donc je ne me pose pas de question mais en fait il nous faisait bifurquer avant une voie interdit au vélo, n'ayant pas vu le panneau je m'y suis engagé... On sort donc un peu plus loin et on continue par le bord de mer, vraiment cool!\n\
\
On arrive a Te puke vers 12h30, on découvre un peu la ville et on attend patiemment 16h.\n\
\
Le rendez- vous se passe super bien, on a le job sans vraiment de condition, c'est nickel ! =) Seul soucis, on a pas de véhicule motorisé et ca va peut être posé soucis pour certains champs éloigné. On commence normalement le 5 novembre!\n\
\
On retourne chez Judy et David en faisant un petit arrêt sur la plage:).",
            },
            "02/11/2019": {
              km: "0",
              text:
                "Je me lève vers 5h pour aller voir le levé de soleil vivement conseillé par David. C'est vrai que c'était plutôt pas mal !\n\
L'après midi David nous propose d'aller faire une petite randonnée pas loin.On monte sur une petite colline pas loin, vraiment grandiose d'avoir une vu sur toute la bai !\n\
\
On finit cette sortie sur une petite bière dans un bar, je vois qu'il vende des Buns...Des souvenirs Japonais me forcent à en prendre un.Bon mais pas ouf comparé a nos excès au Japon ^^.",
            },
            "03/11/2019": {
              km: "17",
              text:
                "L'afrique du sud a gagné! Judy est super contente, c'est son pays d'origine.\n\
\
Il fait super chaud aujourd'hui! On voulait aller manger sur la plage mais la chaleur nous redirige vers un parc.\n\
\
Vers 15h c'est le départ pour Te puke !\n\
Sur la route on se dit que ce serait con de ne pas profiter de ce temps pour faire notre première baignade ! On s'arrête juste avant de s'enfoncer dans les terres, on amène les vélos chargés sur la plage qui semblaient vide vu son immensité les précédentes fois qu'on y est venu mais en ce dimanche avec cette chaleur, on est plus les seuls à en profiter ! Elle est froide mais on est quand même hyper content d'y être allé.\n\
On repart sous ce soleil de plomb, un peu rafraichit.\n\
\
A Te puke, on va vers le seul camping. Les avis sont exécrable  mais ca n'a pas l'air cher et c'est le seul endroit ou on peut aller... Mais finalement pas mal d'avis disent aussi que ca va mieux maintenant puisque les propriétaires ont changés ! Et effectivement c'est vraiment nickel maintenant =).",
            },
            "04/11/2019": {
              km: "0",
              text:
                "Détente et courses, pas vraiment plus. On attend de savoir quand va commencer le boulot ! Ca dépend de l'état de floraison des fleurs de kiwis, apparemment soit demain soit après demain !",
            },
            "05/11/2019": {
              km: "20",
              text:
                "On est retourné à la plage ! Le soleil tape fort, mais on a fait attention.",
            },
            "06/11/2019": {
              km: "0",
              text: "On commence toujours pas a récupéré des fleurs...",
            },
            "07/11/2019": {
              km: "30",
              text:
                "Ca y est c'est le début ! Et c'est vachement loin ! 1h30 de vélo pour y aller...Et grâce à Camille on était à l'heure... On est arrivé à 7h30 pour 8h30... ^^\n\
Et du coup on doit récupérer les fleurs mâle fermés parce que c'est elle qui contiennent le plus de pollen. On prend rapidement le coup de repérer les fleurs mâles des femelles. Cette première journée est tranquil, le soleil tape mais on est sous les arbres donc c'est agréable d'être en exterieur :).\n\
\
On fait la rencontre d'un couple de Français et on mange avec eux le soir. Une super soirée!",
            },
            "08/11/2019": {
              text:
                "Faut qu'on ramène les sacs de fleur à leur usine, sur le vélo c'est compliqué donc on essaye de trouver des gens pour nous accompagner...Sachant pour info qu'on ramène entre 10 et 30 kg de fleur par jour et par personne !\n\
\
Aujourd'hui tout le monde essaye d'être rentable mais ils récoltent énormément plus que nous et on arrive pas a dépassé le seuil pour gagner plus.",
            },
            "09/11/2019": {
              text:
                "On se dit qu'il faut mettre les bouchés double et on y arrive ! On gagne 200dollars au lieu de 150 pour une journée de 7h30. (Globalement 120€ brut)\n\
\
Je vais pas vous raconter combien on gagne chaque jours, je trouve pas ca très intéressant ^^.Donc le poste suivant c'est pour dans 1 mois ;) !",
            },
            "10/11/2019 -> 21/11/2019": {
              text:
                "C'était... Intéressant de cueillir des fleurs mais je referai pas ^^. Mal au cou, mal au dos, mal au bras... On a de la chance avec nos boulots d'ingé ;) !\n\
On a aussi eu quelques péripéties de camping... Le seul camping de Te puke(la ville dans laquelle on travaillait) n'était pas ouf : cher, les gérants étaient très méprisant, moqueur et jamais disponible. On a changé pour un emplacement que proposaient nos employeurs et c'était tellement humide qu'en seulement 5 jours la tente à moisie...\n\
Par contre se poser ca permet de faire des rencontres ! dès le premier jour on a sympathisé avec un couple de Français qui sont arrivés y'a un mois, Pauline et Louis, des nordistes. On s'est dit qu'on essaierait de se recroiser !\n\
Même si c'était pas facile, globalement on est content de ce taff, on a réussi à se faire environ 1200€ en 2 semaines de travail !\n\
Avant le vrai départ, on retourne vers Tauranga pour déposer le vélo de Camille et pour qu'elle prenne un bus vers Hamilton(là ou elle va commencer le trek). Après ce au revoir difficile je repars en solo en direction du Cap Est, l'endroit où le premier levé de soleil du monde à lieu (en fait non, y'a encore des îles plus à l'est, mais c'est vendu comme ça) !",
            },
          },
        },
        "05-east-cape": {
          pre: {
            title: "En route pour le East Cape",
            path: "/travels/new-zealand/east-cape",
            name: "nz5",
            "date-inter": "22/11/2019 - 03/12/2019",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "nz4",
            "next-link": "nz6",
          },
          data: {
            "22/11/2019": {
              km: "15",
              text:
                "Camille est partie pour Hamilton ce matin. Ca fait bizarre après avoir passé 2 mois sans se quitter plus de quelques heures...\n\
Je passe une dernière nuit dans la région pour faire une dernière soirée avec les copains du flowers picking. J'ai trouvé un petit magasin qui vend des spécialité de chez nous, on a partagé ca sur la plage, c'était très sympas !",
            },
            "23/11/2019": {
              km: "55",
              text:
                "Et c'est parti ! Petite journée en théorie donc je profite du wifi avant de partir vers 10h. Je sais pas ce que j'ai ajouté de plus depuis qu'on s'est arrêté mais j'ai les sacoches blindé... Y va vraiment falloir que je fasse un tri de tout ce bordel... Un peu tard vu que Camille vient de laisser des affaires à Tauranga, j'aurai du en profiter plus.\n\
\
Les km défilent, ça va vraiment mieux que lors de notre départ y'a 1 mois. Je repasse par Te Puke pour la dernière fois! Et je continue sur un route qu'on a déjà pris lors de notre premier jour de cueillette.\n\
Vers midi, je mange au bords de mer dans un petit bosquet à l'abri du soleil qui tape vachement fort maintenant que l'été est là ! Depuis mon spot je repère un petit chemin qui part dans la direction où je vais... Ça à vraiment l'air d'un plan foireux mais c'est toujours comme ça qu'on en profite le plus ! ^^\n\
Je pars la dedans et je l'avais bien deviné, le chemin est relativement praticable 1km et devient finalement du sable pour même bifurqué et aller vers la plage. Après 2mins d'hésitation, je me dis autant continuer jusqu'au prochain accès à la route... Et c'est parti pour 1km à pousser le vélo....Mais arrivé là je vois qu'il y a une débouché de rivière et après examination je ne vais pas risquer la traversé donc demi-tour. Au moins maintenant je sais que pousser le vélo dans le sable c'est chiant ^^.\n\
J'arrive à Matata dans un camping très sympathique toujours en bord de mer :).\n\
Sinon, le savon ça ne marche pas pour dégraisser une chaîne de vélo !\n\
La journée ayant commencé assez tard et finit assez tôt malgré mon plan foireux, je vais tenter d'augmenter le kilométrage demain !",
            },
            "24/11/2019": {
              km: "85",
              text:
                "C'est vraiment long 85 km ! J'ai du mal à croire que ce matin était ce matin ^^.\n\
Réveil tôt pour partir tôt! Toute la matiné les km ont défilés sans que je les vois mais arrivé à midi, pas moyen de trouver un bon endroit pour manger...Je m'arrête donc à l'arrache sur le bord de la route et 20 mins après c'est reparti !\n\
A part une petite colline dans la matinée, j'avais vu que c'était plat mais finalement c'est plutôt valoneu et ca me casse pas mal, mais il est 13h et j'ai déjà fait 55 km...Mais je me suis réjoui trop vite...Un accident oblige un détour (pas parce que je ne pourrais pas passer en vélo mais parce qu'il y a des morts et aucun moyen de négocier le passage...Donc obligé de faire 10 km de détour et 200m de dénivelé en plus....\n\
\
C'était ma grosse réflexion de la journée, ici ils sont hyper stricte sur la sécurité et les risques quel qu'il soit. Tous les locaux à qui je parle me sorte une phrase lié à ça: je DOIS être plus visible et porter un habit fluo, je DOIS faire très gaffe aux vols parce que les gens n'aiment pas les Maoris (à tort ou à raison, je sais pas), je DOIS toujours être en camping parce qu'on ait plus en sécurité... Au début je ne le remarquait pas vraiment mais maintenant ça me choque presque et ça me soule un peu ^^. C'est cool de prévenir mais j'ai l'impression qu'ils veulent vraiment que j'applique ce qu'ils me disent de faire...\n\
\
Je prends le détour et... Une route de montagne qui monte avec la circulation d'une autoroute... C'est vraiment pas ouf. Et pour en remettre une couche, j'arrive a un croisement et un gars responsable de la déviation me dit qu'il va falloir que je passe par une route de gravier...Avec toutes les autres voitures... Finalement après négociation il me dit de prendre la route classique, parce que c'est plus sûr, et pour le coup je suis plutôt d'accord pour appliquer ce qu'il me dit de faire ^^.\n\
\
Les derniers km sont vraiment long et je rate l'occasion de prendre un trail au bord de la mer parce que mon vélo chargé m'empêche de passer la barrière limitant l'accès aux piétons et vélos... Un peu déçu mais ca me permet de faire plus vite les derniers km pour finalement me poser à Opape Motor camp.",
            },
            "25/11/2019": {
              km: "56",
              text:
                "Ca y est c'est aujourd'hui que j'ai eu un nouvelle avant goût de ce que me réserve la NZ... C'est jamais plat !\n\
\
Dès le matin avec pas mal de fatigue de la veille, ca commence par 300m... Le début est vraiment pas facile, je m'arrête même pour vérifier si le vélo n'a pas un soucis et heureusement ce n'est que le moteur qui n'était pas encore chaud. Arrivé en haut j'ai droit à une super vue !\n\
La journée avance tranquillement mais y'a un autre soucis : J'ai pris des petit coups de soleil hier et je supporterai pas de mettre un habit à manches longues. Ca commence un peu à picoter. J'essaye de prendre des pauses à l'ombre de temps en temps mais j'aimerai éviter que ça s'aggrave vraiment et mes jambes me disent qu'une petite journée leurs feraient du bien.\n\
Du coup j'écourte mon objectif pour m'arrêter à Te kaha. Demain j'essaierai  de ne pas rouler entre 11h et 14h :).\n\
\
C'est incroyablement vide depuis que j'ai dépassé la ville de Whakatane.Peu de véhicule sur la route et les campings sont vides ! C'est encore le début de saison mais les campings proches de Tauranga étaient déjà blindés. C'est une région très Maori, ca doit être aussi l'une des raisons pourquoi les gens ne viennent pas très souvent.\n\
\
Je dois vous avouer que la plupart des photos que j'ai prise aujourd'hui, j'étais toujours en train de rouler, c'est pour ça qu'on voit souvent les barrières du bord de la route ^^.",
            },
            "26/11/2019": {
              km: "65",
              text:
                "Aujourd'hui tout va mieux, les jambes ont pu se reposer et c'est très nuageux donc les coups de soleil vont moins s'aggraver !\n\
\
Mes réserves de nourriture s'amenuisent donc je vais faire le petit dej dans le petit snack du coin et ils n'ont plus de donut... Obligé de prendre un burger ^^. Plein d'excès pour commencer la journée, je suis paré!\n\
\
Après une dizaine de km je passe un pont et il y a un accès à une rivière...Ca aurait été parfait pour passer la nuit mais il est 10h... Je suis pas motivé pour attendre la nuit ici ^^.Surtout que j'ai pas de réseau et j'ai complètement oublié de télécharger de la musique, ca commence à être embêtant! Je vais traverser pas mal de rivière pendant la journée, j'espère me trouver un coin similaire.\n\
\
J'avais déjà rencontré les sandflies(les moustiques d'ici) depuis mon arrivé, mais maintenant que je suis un peu à l'écart des villes elles sont encore plus présentes, je me fais bouffer à chaque arrêt! D'ailleurs je me demande pourquoi ces bestioles n'ont pas évoluées pour ne pas piquer les humains... Leurs piqûres / morsure se sent très clairement donc j'ai tout plein de bouton mais la plupart de celles qui m'ont piqué sont écrasées... Apparemment c'est sur l'île du sud qu'il y en a vraiment BEAUCOUP... J'ai pas hâte.\n\
\
Ca y est les km portent leurs fruits, il est 14h30 et je suis encore en forme (à moins que ce soit le Nutella que j'ai acheté ce matin ?), mais j'ai envie de me trouver un petit coin tranquil plutôt que de dormir en camping. Vers 15h30 je commence a cherché et je passe à côté de plein de terrain superbe mais privé... Après 10 mins d'hésitation devant un portail moitié ouvert, j'abandonne mon idée de squatter sans autorisation. Je vois que le premier camping est 25 km plus loin au cas ou mais ça m'enchante pas tellement.\n\
Finalement après un pont, je vois des coins qui semblent plutôt correct, je descends dans le lit de la rivière, la traverse en poussant le vélo, et 20m plus loin, un petit coin entre les arbres qui fera parfaitement l'affaire =)",
            },
            "27/11/2019": {
              km: "50",
              text:
                "M'étant endormi à 21h hier, faute de prévoyance quant à mes occupations, je me réveil vers 5h du matin, une bonne préparation pour le réveil du lendemain!\n\
\
Je remballe tout et me voilà reparti, je n'ai que 25 km à faire pour arriver à Te Araroa, la petite ville avant la route vers le cap est. La route se passe vraiment bien, j'enchaine vraiment bien les km, je suis même impressionné de la différence de ressenti en si peu de jours !\n\
\
Arrivé à la ville j'ai enfin du réseau... Je plaide coupable j'ai passé la matiné sur le téléphone.\n\
\
En allant faire des courses je croise un couple Tchèque - Italien qui sont aussi à vélo et ils vont au Cap demain matin:). Eux préfèrent se lever à 3h du matin plutôt que de tenter d'aller dormir sur place, beaucoup trop de motivation ^^. Un peu plus tard, je les vois discuter avec un Anglais, lui aussi en vélo! Alors que je n'en ai pas croisé un seul en 5 jours, voilà 3 cyclotouriste d'un coup ! ^^ On se retrouvera tous demain à côté du phare =).\n\
\
Je repars en direction du cap sur une route indiqué par mon GPS comme \"non revêtue\"... Avec 6h pour faire 20km... Mais comme les voitures y passent ca doit sûrement être plutôt praticable.\n\
\
Pendant 10 km ca alterne entre un peu de gravier et une route génial avec une vu ouf et un vent dans le dos qui m'aide beaucoup et que je redoute vraiment de face demain :/. Les 10km suivant sont uniquement du gravier/caillou/sable... Le vélo part dans tous les sens mais ca se fait sans trop de soucis surtout avec cette aide monstrueuse du vent !\n\
\
Arrivé sur place j'hésite un moment à me caler près de la mer sur un terrain qui semble privé mais y'a des gens qui travaillent et je semble pas le bienvenue. Mais finalement en allant leur parler ce sont les propriétaires et ils m'indiquent un endroit ou poser ma tente =) .\n\
\
En me trouvant une place je rencontre Robert, un allemand en camping-car. On parle un petit moment et il m'invite a partager un repas avec lui, c'était super sympas! =)\n\
\
Réveil à 4h45 demain.",
            },
            "28/11/2019": {
              km: "25",
              text:
                "Le réveil était difficile et la montée jusqu'en au de la colline où se trouve le phare aussi mais ca valait le coup! Finalement, on était beaucoup à 5h du mat à attendre l'apparition de la grosse boule jaune, ça a pris son temps. 6h du matin tout le monde s'en va, je descends également mais pas moyen de retrouver Robert qui avait fait la montée avec moi. Arrivé en bas, je vois sont mobilehome partir et de retour à la tente je trouve un petit mot à l'entrée de ma tente =).\n\
\
Je prends le temps de me poser et de me préparer psychologiquement pour le retour venteux et sur une route pourri...\n\
\
Le premier km est pas facile mais après ça j'arrête de me dire à quel point c'est horrible et ca avance tout seul ! J'arrive pour 9h30 à Te Araroa alors que je pensais que ca allait me prendre la journée et je me dis que je vais quand même rester sur mon plan.\n\
\
Je me dirige donc vers le camping. Et finalement ce que je pensais être un camping est une maison, c'est des gens qui ont aménagé leur jardin et leur maison pour pouvoir faire auberge de jeunesse et camping, c'est très sympas !\n\
\
On discute pas mal avec Camille qui prévoit finalement de ne pas continuer le trek dans l'immédiat, c'est beaucoup beaucoup beaucoup plus dure que prévu et ici il n'autorise jamais le camping sauvage, donc c'est aussi très cher parce que les gens profitent que tout le monde passe par ce chemin... On décide de se retrouver à Gisborne à partir de samedi pour quelques jours (ce qui me laisse 2 jours pour faire 170km et 1400m de dénivelé :o) !\n\
\
Arrivé au soir, je fais une partie de billard avec un allemand (y'a énormément de jeune allemand ici, c'est répandu de partir juste après le bac pour 1 an chez eux) et je discute avec 2 Français en van qui sont arrivé en même temps que nous et qui vont vers Gisborne aussi, je crois qu'ils m'ont proposé subtilement de me faire une parti du trajet mais je suis motivé!",
            },
            "29/11/2019": {
              km: "115",
              text:
                "Mon objectif est à 80 km et 850 m de dénivelé, je me presse pas pour partir comme j'arrive toujours à 15h généralement, cette fois j'espère remplir la journée en ayant commencé tranquillement à 9h!\n\
\
La journée de pause m'a fait du bien, mais la journée commence par 600m de dénivelé dans les 15 premier km, ca casse pas mal mais j'ai encore de la route et je veux pas trop trainer ! Au milieu de la journée un paon vole au dessus de la route devant moi... Normal apparemment ^^.\n\
\
Finalement même en ayant pris une bonne heure de pause pour manger j'arrive à destination pour 15h... Un jour j'arriverai à estimer correctement mon temps de trajet...\n\
\
Je me pause quelques minutes et j'hésite à continuer un peu, pendant ce temps un camping-car se gare pas loin d'où je suis posé et un local vient me parler, on discute un petit moment, et en lui disant que j'hésite à continuer parce qu'il y a une montagne juste après il me propose de me monter la haut parce que c'est sur sa route, j'accepte volontier ! On prend un petit thé et il m'avance de 20km =). (J'avoue les 115km c'est avec 20km en voiture ^^) Un grand merci à lui !\n\
\
Je continue 15 km pour arriver à Tolaga bay, j'espérais trouver un endroit où me poser sur la route mais rien à l'horizon... Arrivé en ville le seul camping coûte 20$, j'ai pas envie. Je me trouve un buisson sur le bord de mer, un peu caché et voilà une nuit gratuite !",
            },
            "30/11/2019": {
              km: "60",
              text:
                "Réveil matinal pour sortir de mon buisson pas trop tardivement. Tout est prêt lorsque je reçois un appel qui va changer ma vie ! C'est le cher Benj en compagnie de deux amie à nous, en direct de Nancy. Après quelques préparatifs, nous avons fait un sandwich de la Terre ! Un moment historique plein d'émotion <3 ^^ !\n\
\
Ca repars en direction de Gisborne ! Même si aujourd'hui 50km à faire, pas besoin de se presser mais le temps est très menaçant et puis je dis pas non à un douche et à une aprem détente dans un vrai lit !\n\
\
Mais décidement je suis nul pour estimer les distances... 2h30 plus tard, à 11h20 je ne suis plus qu'à 5km de ma destination... Et le check-in est à 14h... Je me pose sur le bord de mer pour manger et attendre que la journée avance.\n\
\
Le soir je vais chercher Camille et on se fait un petit resto indien, c'était son anniversaire hier !",
            },
            "01, 02, 03/12/2019": {
              km: "0",
              text:
                "Quelques jours de repos à prévoir la suite de notre aventure !\n\
\
La ville n'a rien de passionnant : une route principale avec des commerces en tout genre et une rivière qui la traverse.",
            },
          },
        },
        "06-gisborne-taupo": {
          pre: {
            title: "De Gisborne à Taupo",
            path: "/travels/new-zealand/gisborne-taupo",
            name: "nz6",
            "date-inter": "04/12/2019 - 26/12/2019",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "nz5",
            "next-link": "nz7",
          },
          data: {
            "04/12/2019": {
              km: "70",
              text:
                "Camille a repris le bus ce matin, elle repart marcher depuis là où elle s'est arrêté, ce repos lui a fait du bien et a permis à ses ampoules de guérir!\n\
\
Mes plans n'ont pas changé pour le moment, je me dirige vers Taupo en passant par Whakatane (qui se prononce Fakatane) et Rotorua, les gorges des 100 premiers km sont apparemment magnifique !\n\
\
La matinée est vraiment longue, j'ai un vent de face hyper violent, j'ai du mal à passer le 3ème plateau alors que c'est uniquement avec que je roulais les jours précédents... Surtout que le début n'a rien d'impressionnant, des plaines jonchées de petites collines par-ci par là.\n\
\
L'après midi c'est encore pire ! Ca commence à monter, le vent est toujours de la partie et la pause repas m'a complètement cassé... Dans la douleur j'arrive au village de Matawai où je passe la nuit au camping.",
            },
            "05/12/2019": {
              km: "35",
              text:
                "Départ difficile dû à des courbatures de la veille et le début ne me ménage pas, ça monte de ouf et le vent est toujours de face! Mais le paysage se transforme progressivement en une vallée très verdoyante. J'arrive enfin en haut et la descente est un plaisir ! Elle dure un moment jusqu'à atteindre la rivière pour s'engouffrer dans les gorges qui sont vraiment très sympas.\n\
\
J'ai bien envie de m'arrêter tôt et une occasion se présente, j'emmène le vélo dans un petit chemin qui descend sur le lit de la rivière après quelques mètres je pense à la remontée de demain... Je rempli donc mon sac à dos avec le nécessaire pour la nuit.\n\
\
Pas vraiment d'endroit où poser la tente mais ca va le faire ! Je me trouve un petit coin sur des rochers pour dormir à la belle étoile, normalement pas de pluie cette nuit.\n\
\
Petit bain dans la rivière, elle est fraîche mais c'est agréable :).\n\
\
Toujours beaucoup de vent en début de soirée mais il se calme progressivement jusqu'à plus du tout pendant la nuit :).",
            },
            "06/12/2019": {
              km: "90",
              text:
                "J'ai super bien dormi ! Je repars décidé à faire des km aujourd'hui!\n\
\
Un début tranquille puisque ca descent majoritairement mais c'était sans compter sur le vent de face qui me ralenti continuellement...\n\
\
Je retrouve la route que j'avais emprunté 2 semaines plus tôt pendant le reste de la journée. En arrivant à Whakatane je passe par un magasin de vélo pour quelques emplettes et je croise un cycliste à qui je dis qu'il me reste encore 20km aujourd'hui qui me dit que j'ai pas besoin de me dépêcher pour aller vers Rotorua puisque c'est seulement 80km majoritairement plat (spoiler : je me suis haï de lui avoir fait confiance). Je prends donc le temps de me faire un petit KFC et je prends un camping dans la ville =).\n\
\
Mine de rien le matériel ca s'use... Ma tente a vraiment morflé d'être resté 3 semaines au même endroit sans séché, de même pour mon matelas et je découvre un trou dans mon camel bag...J'essaye de raffistoler ça, j'espère que ça tiendra.",
            },
            "07/12/2019": {
              km: "80",
              text:
                "Je me réveil dans l'humidité comme jamais... Alors qu'hier au bord de la rivière j'ai été étonné à quel point c'était sec. Je comptais partir tôt mais j'attends quand même le temps que tout ait à peu près séché ce qui me fait partir vers 9h.\n\
\
Un début relativement plat même si toujours un peu de vent de face mais après 30 km, voyant que ça commençait à monter, je check le dénivelé sur une application... En fait le cycliste m'avait dit n'importe quoi (ou j'ai mal compris ou pour lui ça c'est plat et facile...) ! J'ai 500m à monter sur quelques km... Un dénivelé de 1000m sur la journée... Et arrivé dans cette partie plus un seul brin d'air, j'étouffe, je dégouline de ouf ! Mais arrivé en haut ca valait le coup, un super lac!\n\
\
Je fais ma petite pause repas puis je repars voyant que la météo se gâter ...J'ai pas fait 500m que c'est l'apocalypse ! À peine le temps de tout ranger à l'abri dans les sacoches... Une fois que plus rien ne craint ben il faut continuer :).\n\
\
Et en vrai c'est tellement plus agréable de rouler sous la pluie que quand il fait hyper lourd et chaud. Mais ce n'est pas l'avis de tout le monde, et les gens sont trop gentils : une voiture s'arrête et me propose de m'emmener. Évidemment j'accepte, on met le vélo à l'arrière et c'est partie pour Rotorua. C'est tout une famille qui habitent dans les alentours de Whakatane. Arrivé à Rotorua je me trouve une auberge pour la nuit, ca coute pas beaucoup plus cher et c'est au sec !\n\
\
Un truc que tout le monde dit sur Rotorua c'est que ca pu le soufre dans les rues et c'est vrai. Mais à part ça je ne trouve pas d'endroit vraiment sympas et je ne suis pas hyper fan de la ville.",
            },
            "08/12/2019": {
              km: "80",
              text:
                "Une journée riche en rebondissement !\n\
\
À Gisborne on avait prévu avec Camille de se retrouver dans le Timber Trail, un parcours en vélo que la Grande randonnée que fait Camille traverse. Je me dirige donc vers l'entrée du trail à environ 90km de Rotorua. Départ 9h tout sec et j'ai peur de la météo mais bon tant pis on y va !\n\
\
La route est vraiment sympas, ca descend et je pensais me prendre la pluie mais finalement rien, c'est vraiment la météo parfaite !\n\
\
Après 30 km à monter et descendre avec une tendance qui descend, une voiture me double en klaxonnant de façon insistante, je me dis que c'est des gens que je connais mais la voiture ne me dit rien les passagers non plus (un homme et un petit garçon d'après ce que j'en ai vu)... Il y a un renfoncement un peu plus loin, la voiture s'arrête... Je me demande qu'est-ce qu'il peuvent bien me vouloir, je m'arrête. Et là de la voiture, je vois sortir Camille...... :o elle était censé être au milieu du Trail... Je suis perdu... Elle me dit 'je vais à Rotorua, fait demi-tour et viens' et 'le gars est pressé je veux pas le faire attendre' et elle repart dans la voiture.\n\
\
Je suis resté 30 secs bloqué à ne pas comprendre ^^. En fait elle a été malade après être rentré dans le trail donc elle a fait demi-tour pour avoir un endroit un peu plus accueillant pour se reposer.\n\
\
Mais je refais pas les 30 km dans l'autre sens c'est mort ! Donc on se dit qu'on se retrouve à Taupo, c'est seulement à 1h de bus de Rotorua et il me faut 50km pour y arriver :).après 1/2h de route, la même pluie que la veille qui se calme progressivement après 40mins. Je traverse un moment un barrage et je vois un petit endroit à l'abri, j'y vais pour manger une barre de céréales avant de repartir. Et entre tant la pluie s'arrête! Je repars motivé !\n\
\
Mon GPS veut me faire prendre un chemin de terre à travers la Forêt...Ca me tente pas.Je choisi l'autre solution, la national 1... Au moins la route est bien et même si les voitures vont vite et passent pas loin, elles me voient de loin.\n\
\
Je pensais manger à Taupo mais je fais une pause avant pour cause de manque d'énergie, en espérant qu'il ne se mette pas à pleuvoir à se moment là. Et j'ai de la chance, la pluie recommence 1/2h après mon arrêt et celle là je vais la retenir... Je voyais pas à 10m, la route était submergé d'eau... Encore 25 km jusqu'à Taupo... Mais bon, y fait pas si froid que ça, la route descend donc j'avance bien et au moins les voitures passent à 2m de moi avec ces conditions ^^.\n\
\
Arrivé à Taupo la pluie s'est arrêtée juste pour que je puisse faire une photo depuis le point de vue un peu au dessus de la ville. J'attend ensuite Camille qui arrive en bus et on va récupérer notre chambre avant d'aller manger dans un superbe resto indien.\n\
\
Petit bémol, la deuxième pluie était si violente que tout est trempé dans mes affaires malgré les sacoches étanches... Ca tombe bien qu'on ait une chambre pour tout faire sécher.",
            },
            "09/12/2019": {
              km: "0",
              text:
                "Bon y faut changer les plans. Et profiter un peu de Taupo, c'est vraiment magnifique comme ville! Enfin la ville en elle même n'a rien de particulier mais le lac juste devant avec la montagne dans le fond... C'est vraiment spectaculaire.",
            },
          },
        },
        "07-canoe": {
          pre: {
            title: "Notre expédition en canoë",
            path: "/travels/new-zealand/canoe",
            name: "nz7",
            "date-inter": "10/12/2019 - 17/12/2019",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "nz6",
            "next-link": "nz8",
          },
          data: {
            "10/12/2019": {
              km: "50",
              text:
                "Départ pour Turangi! Camille prends un bus et moi je fais le trajet en vélo. La route longe le lac pendant un bon moment puis monte progressivement sur un plateau avec une vue superbe sur le Tongariro(volcan super connu du coin, le pic enneigé dans le fond des photos du lac Taupo), pour finalement redescendre au bord du lac et suivre la rive jusqu'à Turangi. Il me faut 3h pour faire le trajet et rejoindre Camille dans un petit park.\n\
\
On prévoit de se poser en camping sauvage au bord d'une rivière mais finalement on a envie d'un peu de confort et on choisit d'aller passer la nuit dans une hutte trouvé via airbnb à quelques km de là.",
            },
            "11/12/2019": {
              km: "67",
              text:
                "On prend notre temps ce matin avant de partir pour Taumarunui. Aucun moyen de s'y rendre pour Camille donc elle va faire du stop. Et elle trouve très rapidement quelqu'un et arrive à destination alors qu'il me reste encore les 3/4 à faire...\n\
\
De très beau paysage... Par contre ca monte de ouf ! J'ai 800m de dénivelé à faire presque sans pause avant d'avoir toute la redescente. Et une superbe redéscente avec une vue sur le Tangariro d'un angle différent, il est tellement enneigé à cette époque de l'année(équivalent 11 juin en France). Je fatigue pas mal sur la fin mais ca va quand même vachement plus vite que la matiné.\n\
\
Arrivé à destination on réserve un canoë pour les 7 prochains jours. On nous donne 5 bidons étanches pour nos affaires et la bouffe et on pose la tente sur le camping des loueurs de canoë. Y faut encore aller faire les courses pour une semaine et tout est bon.\n\
\
Petite plus avec leur installation douche en extérieur et un chauffe eau au gaz, c'est trop bien la douche chaude en pouvant voir le ciel ^^.",
            },
            "12/12/2019": {
              km: "32",
              text:
                "Rdv 8h30 pour les explications! On a droit a tout pleins d'explication imagé en vidéo et agrémenté d'une boisson chaude, d'un pain super bon et d'une petit touche d'humour.\n\
\
On descend ensuite vers les bateaux avec nos bidons qu'on chargent et attachent au canoë, et après 10mins de formalités, c'est parti!\n\
\
Départ 10h40, c'est trop agréable comme moyen de transport !\n\
\
Pour midi, petite pause sur une plage, le temps de finir de sécher la tente, et d'entamer nos réserves de bouffe. J'ai eu l'impression de prendre beaucoup mais finalement ça nous paraît peu... On verra bien les jours suivants :).\n\
\
On est vraiment impressionné, on a croisé personne de la journée, on est vraiment seuls sur la rivière, c'est trop bien !\n\
\
On avance bien et on arrive à destination presque 2h avant les estimations.\n\
\
Le seul bémol de cette expédition c'est le prix des campings qui sont obligatoire, impossible de faire du camping sauvage comme d'hab...Donc 12€ par nuit et par personne pour des toilettes qui ne sont juste qu'un trou surplombé d'une cabane et un abri qui recueille l'eau de pluie... Mais ca vaut quand même le coup !\n\
\
Petit repas dans l'abri, le tonnerre gronde, on va bientôt prendre la pluie ! Effectivement juste avant de dormir quelques gouttes tombent, pas grand chose. Il nous faut de la pluie pour aller jusqu'au bout ^^.",
            },
            "13/12/2019": {
              km: "32",
              text:
                "Réveil dans un cadre de ouf ! On prend notre temps de tout bien faire sécher, de tout bien ranger et de tout installer sur le canoë. On adore vraiment cette expédition! C'est complètement coupé du monde et c'est hyper relaxant.\n\
\
On commence à arriver dans une zone un peu plus canyon qui nous laisse moins d'endroit ou s'arrêter mais on arrive quand même à négocier quelques pauses.\n\
\
On a vu sur la carte, qu'on nous a donné, une cascade pour une pause déjeuner, on s'y arrête pour se faire notre petit casse croute.\n\
\
Pour l'instant on n'a pas encore subi de grosse gamelle, on croise plein de rapide mais on arrive a bien s'en sortir pour limiter les dégâts ^^. On est resté coincé sur un rocher en étant perpendiculaire au courant un moment, on a cru rester coincé mais on s'en est sorti sans problème!\n\
\
En milieu d'aprem on croise notre premier humain depuis plus de 24h, il nous dit qu'il nous reste 1h30 pour notre camping et on continue.\n\
\
Beaucoup de vent de face dans l'après midi... Peu importe le moyen de transport c'est hyper handicapant ^^.\n\
\
Arrivé au camping vers 16h et des gens sont sur la rive à attendre avec leur canoë, mais sans bidon, quelqu'un vient les chercher quelques minutes plus tard en bateau à moteur (\"jet boat\" en anglais c'est vachement plus simple à dire !), on a donc encore le camping pour nous seul !\n\
\
Malgré le gros panneau interdit de faire un feu, on a acheté des marshmallows pour ça et y'a pas mal de marque d'ancien feu donc on se sent moins coupable ^^. Après une tentative infructueuse de ma part, Camille arrive à l'allumer =). Mais petit bémol, les chamallows ne sont pas aussi bon que prévu et ils sont même moins bon une fois fondu :/. Mais c'était sympas de faire un petit feu !",
            },
            "14/12/2019": {
              km: "36",
              text:
                "On traine pas mal ce matin alors qu'on avait prévu de partir plus tôt que la veille ^^. Et la journée est censée être plus longue...\n\
\
On essaye d'avancer avant l'après midi et l'arrivé du vent. On est assez efficace et on avance bien. Arrivé à l'heure de manger, on met un petit moment avant de trouver un endroit correct pour s'arrêter, c'est vraiment devenu un canyon de 50m de large ^^.\n\
\
L'après midi avance tranquillement, et on se dit qu'on devrait plus être très loin quand on voit quelqu'un en hauteur sur la paroi du canyon... En fait, c'est là le camping, et heureusement que cette fille était là, on l'aurait complètement loupé sinon!\n\
\
Le niveau de la rivière est bas donc on doit monter le canoë haut pour le mettre à l'abri et la rive n'est pas très accueillante pour ça...Après ça y'a 50m à monté avec des bidons assez lourd. Enfin une fois tout installé c'est vraiment un petit coin de paradis!",
            },
            "15/12/2019": {
              km: "32",
              text:
                "On part de bonne heure ce matin! On passe par l'attraction touristique du coin, \"bridge to nowhere\", un pont construit y'a quelques dizaines années déjà mais vers une zone qui a été abandonné juste à la fin de la construction du pont, mais on prend pas le temps de faire les 45 mins de marche pour l'atteindre, on préfère continuer.\n\
\
Et ca tombe bien parce aujourd'hui y faut ramer! On croise très peu de rapide, c'est beaucoup d'eau calme. On veut quand même prendre le temps de se faire une pause repas digne de ce nom mais on se fait tellement bouffer par les sandflies qu'on se dépêche de repartir...\n\
\
On arrive au camping vers 16h, il fait encore assez lumineux et ca tombe bien parce que c'est vraiment le plus beau qu'on ait croisé avec une très belle vue sur la suite de la rivière!",
            },
            "16/12/2019": {
              km: "0",
              text:
                "On se lève tôt et ça tombe bien, 2mins après qu'on ait désinstallé la tente il se met à pleuvoir! Et ça ne s'arrête pas... Donc on part sous la pluie. C'est pas une pluie très violente mais ca mouille quand même.\n\
\
Aujourd'hui on est censé passer par les plus gros rapide du parcours.... Ben pas ouf en vrai... Peut être parce que la rivière est basse mais on a pas su les différencier des rapides qu'on a passé les jours précédents.\n\
\
On dépasse Pipiriki qui marque la fin de la réserve de la rivière et le début du retour à la civilisation, même si y'a toujours pas de réseau, il y a maintenant une route qui longe la rivière et des fermes sur les rives.\n\
\
On veut avancer puisque le dernier jour la marée peut nous embêter et nous obliger a partir dans l'après midi. Ca nous permettra de pas finir trop tard le dernier jour !\n\
\
La météo ici, c'est vraiment imprévisible... Il pleut sans nuage dans le ciel, la pluie s'arrête et on a 15 minutes de soleil mais les nuages reviennent et ça repleut...On a eu ça 3 ou 4 fois dans la journée.\n\
\
La pluie ca créer une ambiance différente et ca met de la brume dans les gorges c'était pas une mauvaise chose et comme disait Camille : \"on aurait pas eu l'expérience complète sans la pluie!\" ^^\n\
\
Plus de rapide aujourd'hui ça avance plus vite! Mais on fait quand même bien plus de km donc on arrive vers 17h30 dans une hutte qu'on avait repéré. Malheureusement il n'y a que 5 lits et il y a déjà 6 personnes à l'intérieur...On installe du coup notre tente un peu plus loin.\n\
\
Et ce soir super nouvelle... Mon matelas est percé... Le matos souffre vraiment beaucoup ici! On trouve la fuite mais j'ai rien pour la réparé sous la main et malgré la tentative avec le matos du matelas de Camille, ca ne marche pas.... On va s'entasser à 2 sur un matela... Même siça va sûrement pas être le plus confortable.",
            },
            "17/12/2019": {
              km: "45",
              text:
                "La nuit a pas était si terrible même si il a pas mal plu et le problème c'est que ça ne s'arrête pas à notre réveil. On décide de finir le trajet dans la journée, encore une longue journée mais ca va le faire sans problème. On part sous la pluie en espérant que ça s'arrête mais rien à faire on va l'avoir pour la journée...\n\
\
On atteint rapidement l'endroit où la marée commence à nous affecter, ca veut dire plus de rapide et qu'il faut ramer!\n\
\
On rame contre la marée un petit moment et pendant une accalmis on essaye de se faire la pause repas mais les rives sont hyper boueuse, rempli de sandflies, y'a de la merde de chèvre partout, il se remet à pleuvoir et pas à moitié, la marée continue de monter et le bateau se bare... Enfin une pause midi des plus reposantes ^^.\n\
\
On repart très vite, plus que 15km! Et ca y est la marée commence à descendre!\n\
\
On arrive trempé, épuisé mais globalement content de ces 6 jours sur un canoë =).\n\
\
On se prend une petite cabine pour la nuit et on va se coucher!\n\
\
Y'a un distributeur automatique de frite à la cuisine ! ! ! Ca fera notre dîner avec un tablette de chocolat ^^.",
            },
          },
        },
        "08-wellington": {
          pre: {
            title: "Petite pause à Wellington",
            path: "/travels/new-zealand/wellington-first",
            name: "nz8",
            "date-inter": "18/12/2019 - 06/01/2020",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "nz7",
            "next-link": "nz9",
          },
          data: {
            "18/12/2019": {
              km: "0",
              text:
                "Une journée repos et courses à Wanganui. Surtout qu'il pleut toute la journée quasiment... On a du mal à faire sécher nos affaires ^^.",
            },
            "19/12/2019": {
              km: "70",
              text:
                "Départ en direction de Wellington! Je pars en vélo et Camille prend un bus. La route jusqu'à wellington n'a pas l'air ouf, et en plus je vais sûrement me prendre beaucoup de vent... Et en plus ces 3 jours de pluie ont tellement rafraîchi l'atmosphère, on est passé de 25 à 12...\n\
\
J'ai de la chance, le vent est avec moi pour les 50 premier km, Je m'arrête dans la première ville pour m'acheter de quoi faire mon casse croûte et finalement j'opte pour la solution de facilité et je me fais un fast food. Je traîne au chaud pendant 3 bonnes heures avant de repartir pour les 15 dernier km. Avec du vent de face insupportable!!\n\
\
J'arrive dans la ferme de Ross, éleveur de vache à lait qui propose de rester chez lui pour pas trop cher sur l'appli que j'utilise. On passe un petit moment ou il m'explique les ficelles du métier. C'était très intéressant, même si je dois avouer que l'industrie laitière se base quand même sur des principes pas ouf...\n\
\
Je pose ma tente et je passe la soirée dans le garage à l'abri de cet horrible vent ! Je me fais un petit chocolat chaud avec des cookies :3. (J'ai tout le confort sur ce vélo ^^ )",
            },
            "20/12/2019": {
              km: "55",
              text:
                "Il pleut toute la matiné, j'attends que ca se calme pour sortir et tout faire sécher. Départ très tardif vers 13h.\n\
\
Et aujourd'hui le vent ne me fait pas de cadeau... Même s'il est plus de côté que de face(heureusement), c'est quand même pas de l'aide.\n\
\
Petite pause à Levin pour recharger un peu mes batteries avant de faire les 15 dernier km vers un petit camping gratuit(c'est très rare ici), qui est vraiment magnifique mais blindé (du coup pas de photo, trop de monde ^^) ! Au bord d'une rivière et avec tout plein d'espace pour poser la tente :).",
            },
            "21/12/2019": {
              km: "90",
              text:
                "Une grosse journée m'attend pour atteindre Wellington, je pars donc plus tôt qu'à mon habitude pour ne pas arriver trop tard.\n\
\
La route de plaine se transforme petit à petit en une route entre colline et mer très sympathique et après encore quelques km, on aperçoit l'île du sud! Je m'arrête manger avec cette vue :).\n\
\
Après ça j'arrive dans l'agglomération de Wellington... Mon Maps était perdu... Y me faisait faire des zigzags pour avancer, prendre des autoroutes interdit au vélo et je n'ai fait que monter et descendre(mais ca c'est pas la faute du GPS).\n\
\
Mais au moins j'ai visité les alentours et c'est plutôt joli. Arrivé à Wellington!",
            },
            "22, 23/12/2019": {
              text:
                "Objectif trouver un boulot ! Après avoir fait tous les sites... On se rend vite compte qu'il n'y a pas grand chose en cette période et qu'on va vraiment galérer à trouver quelque chose d'intéressant... J'avoue m'être découragé assez vite et je voulais repartir rapidement après le nouvel an.",
            },
            "24/12/2019": {
              text:
                "Pour ce réveillon, on se fait plaisir, on va marcher en ville(je vais acheter un cadeau à la dernière minute :/, j'ai pas du tout eu le temps avant ça !\n\
\
Et on se fait resto super sympas! Ca a une tendance un peu française, le fromage était très bon !\n\
\n\
En rentrant on s'offre les cadeaux ! (Des chaussettes et de la pâte à tartiner ;) )",
            },
            "25-30/12/2019": {
              text:
                "Joyeux Noël! Pas la meilleure période du séjour... On s'interroge beaucoup sur quoi faire. On aimerait continuer ensemble mais on est à cour de dollars et Camille veut dépenser le moins possible ses économies. Donc la solution d'aller directement dans le sud ne lui convient pas.\n\
\
Elle cherche activement des boulots, et moi j'hésite! Je postule pour quelques trucs par-ci par là mais rien de concluant ni qui me passionne de ouf. J'hésite même à faire Uber EAT en vélo pendant un temps mais je me rends compte que c'est beaucoup trop compliqué et que ca rapporte pas tant que ça.\n\
\
On retrouve les Français avec qui on avait sympathisé lors de notre cueillette de fleur, on passe un aprem + soirée sympas avec eux :).",
            },
            "31/12/2019": {
              text:
                "C'est le nouvel an et les prix de toutes les auberges ont doublé... Et on a eu du mal a en trouver une de libre ! On doit changer. Quand on arrive devant la nouvelle auberge ca fait peur, ca ressemble à une ruine, mais l'intérieur est correct.\n\
\
Pour la soirée, il y a de l'animation près du port ! On tente d'aller boire un truc dans un bar mais finalement ils sont tous plein et ça nous décourage. On finit la soirée avec un petit feu d'artifice !\n\
\
Et on décide d'arrêter de rien glander les prochains jours et d'essayer de visiter un peu les coins touristique de la ville ^^.",
            },
            "01-06/01/2020": {
              text:
                "Bonne année!!! On fait l'acquisition d'un frisbee ! Ca fait un moment que je vois des gens en faire dans des parcs, j'étais frustré ^^ !\n\
\
On a fait le fameux musée Te Papa, tout le monde nous avait dit qu'il était incroyable et c'est vrai qu'il y a des reproductions de soldat vraiment impressionnante, mais il est plus petit que celui d'Auckland et je suis pas allé à l'étage dédié à la guerre à Auckland... Donc trop d'attente pour celui de Wellington pour un musée relativement comme les autres.\n\
\
Camille continue activement de chercher un taff et a une réponse de la part d'un café un peu éloigné du centre ville mais ca a l'air vachement sympas !\n\
\
Jusqu'au 6 elle était pas vraiment sûr d'être prise mais finalement on lui dit que c'est quasiment fait, elle peut passer pour voir un peu à quoi ça ressemble et comment ça va se passer :) .\n\
\
Je décide donc le 6 au soir de partir le lendemain, j'attendais parce qu'elle hésitait encore à venir avec moi ! Je prends mon ferry pour le 7 à 8h du mat.\n\
\
Dernière soirée avec Camille, on se fait un petit resto avant de rentrer pour que je prépare toute mes affaires !\n\
\
J'arrive à réparer mon matelas en mettant un patch un peu à l'aveugle!(parce que j'arrive pas à trouver le trou exactement) Et j'ai de quoi réparer ma tente si elle casse encore plus!\n\
\
Un trajet prévu pour durer 1 mois et demi... Un peu triste de partir sans Camille mais ça va être top!",
            },
          },
        },
        "09-nelson": {
          pre: {
            title: "Lancement difficile sur l'île du sud",
            path: "/travels/new-zealand/wellington-nelson",
            name: "nz9",
            "date-inter": "07/12/2019 - 06/01/2020",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "nz8",
            "next-link": "nz10",
          },
          data: {
            "07/01/2020": {
              km: "15",
              text:
                "Je pars tôt pour prendre le bateau.\n\
\
Le bateau part et je peux avoir une super vue sur Wellington !\n\
\
Une fois sorti de la baie de Wellington, ca tangue de ouf et je suis hyper malade... Je dors 2h pour éviter de trop en souffrir et je me réveille quand le bateau entre les montagnes de l'île du sud. Un endroit vraiment magnifique, le bateau avance entre 2 collines.\n\
\
Arrivé à Picton je fais 3 courses puis je pars dans la foulé vers l'ouest ! Après 2km de route je trouve un sentier de VTT. J'y passe la journée, c'est difficile mais c'est vraiment trop bien, ça me donne envie d'en faire plus et avec un vélo plus adapté! Heureusement qu'on a pas commencé le voyage avec ce genre de chose je contrôlais pas le vélo comme maintenant, surtout quand il est chargé comme ça et avec le ravin à ma droite, fallait pas se louper!\n\
\
J'arrive dans un camping au bord de mer avec une colline en face ^^. Faut que je trouve un moyen de purifier l'eau, l'eau n'est pas potable dans la plupart des campings ici.",
            },
            "08/01/2020": {
              km: "25",
              text:
                "Motivation négative ce matin, mon matelas n'est en fait pas bien réparé. La tente prend l'eau et le patch s'est décollé à cause de l'eau...\n\
\
Je pars vers 14h quand enfin la tente a séché pour finalement me reprendre la pluie...\n\
\
Je reprends un peu le trail mais ça me ralenti beaucoup trop donc j'opte pour la route qui reste tout de même dans un super cadre. J'arrive à Havelock ou je me cale dans un camping au milieu de la ville. Je rafistole un peu mieux mon matelas et je met une couverture de survie sous la tente, en espérant que ça marche.\n\
Le coin est connu pour les moules, je vais manger dans un fish and chips un peu plus loin pour goûter ça mais c'est pas trop à mon goût :/.",
            },
            "09/01/2020": {
              km: "75",
              text:
                "La réparation du matelas n'a pas marché hyper bien mais je suis d'attaque pour cette grosse journée, je plis tout et c'est partie !\n\
\
Je me fait un petit dej au bord de la rivière, très sympas !\n\
\
Je repars et je passe Pelorus Bridge, un endroit hyper touristique mais qui donnait grave envi! C'était un genre de canyon mais avec plein d'endroit plat pour se poser ou sauter dans la rivière juste en dessous d'un pont.\n\
\
Je continue, les kilomètres s'enchainent bien jusqu'à une monté que je ne voyais plus finir... Arrivé en haut la descente a durée 3 mins... C'est le désavantage d'être motard dans ce genre de cas... Même à 60 km/h dans la descente, je pédalais pour aller plus vite ^^.\n\
\
Encore quelques km de plus et je fais une pause. C'est à ce moment là... Après avoir déjà fait 45km que je me rends compte que j'ai oublié un sac imperméable au camping...\n\
\
Il est 14h je me lance dans une expédition en stop pour récupérer cette connerie et revenir !(Ce truc m'a rien coûté en France mais ca coute 40€ ici... et puis j'avais envie de tenter ça)\n\
\
Je cale le vélo sur le bord de la route, un peu caché, et commence à lever le pouce. J'ai de la chance un gars me prend assez vite !\n\
\
Un voyageur dans son van qui s'arrête au pont que j'ai passé ce matin. On parle VTT et déforestation, un mec super sympas.\n\
\
Y me dépose donc 20km avant mon objectif, impossible de faire ça à pied rapidement, je repars pour du stop et cette fois ça à pris son temps... 1h plus tard un Australien dont le travail est de transporter des voitures dans la région m'emmène jusqu'à Havelock.\n\
\
Je trouve facilement l'objectif de ma quête et c'est reparti pour la même chose dans l'autre sens. Encore 1h à attendre pour qu'une dame avec une vie assez incroyable et super intéressante s'arrête pour m'emmener jusqu'à mon vélo. Il est 17h, elle me propose de m'emmener jusqu'à Nelson... J'hésite au début mais elle insiste un peu et il en fallait pas plus. Je charge le vélo à l'arrière de sa voiture et j'échappe aux derniers km.\n\
\
Je vais déposer mes affaires dans mon auberge et je repars faire le tour de la ville en appelant Camille, c'est vraiment super beau!\n\
\
Je rentre à l'auberge après une journée bien remplis ^^.",
            },
            "10/01/2020": {
              text:
                "Je me balade en ville, je mange au bord de la rivière et j'achète un nouveau matela !",
            },
            "11/01/2020": {
              km: "50",
              text:
                "Vers 10h je suis devant l'auberge après m'être fait un petit dej et avoir tout rangé. Et un moment important de ma vie a eu lieux ! J'essaye de planifier mon trajet avant de partir quand un gars m'accoste et me dit: \"Hey, Nelson Flat Earth society, are you interested ?\", trop d'émotion d'un coup, je n'ai pas pu répondre et assez rapidement il a enchaîné : \"You are traveling around the country, so you can say it's flat !\"... Et malgré mon âme de platiste je n'ai pas pu résister à dire : \"Yeah, soo flat that it's easy to ride this country !\" ... Je crois qu'il a pas aimé et il est parti avant que je puisse lui exprimer mon intérêt :'(. après coup j'ai regretté ne pas vraiment y être allé ça aurait pu être une bonne expérience. \n Pour ceux qui ne me connaissent pas, je sais que la terre est ronde mais je me suis pas mal renseigné sur la croyance en la terre plate. Pas dans une objectif de moquerie mais plus pour comprendre leur point de vu.\n\
\
Je pars donc vers Richmond, la petite ville en dessous de Nelson pour faire des courses.\n\
Je repars ensuite les sacoches remplies en prenant le Great Taste trail qui longe la côte vers l'ouest. On avance vite sur ce trail mais j'arrive trop tard pour prendre un petit ferry qui traverse une rivière. Une petite heure d'attente et je repars pour faire les 20 dernier km. Au bout de 10 km je trouve un super endroit où camper et je me dis autant finir demain.",
            },
            "12/01/2020": {
              km: "25",
              text:
                "Direction Motueka ! Y'a un centre de parachutisme ou j'espère pouvoir sauter ! Malheureusement arriver la bas je me rends compte que ma license française n'est plus valable et on me dit qu'il y a trop de vent pour que je saute aujourd'hui... Après quelques heures de réflexion dans un parc je me dis que ca va me couter trop cher et que ca ne vaut pas le coup. Donc je repars vers un camping gratuit 10km à l'extérieur de la ville.",
            },
            "13/01/2020": {
              km: "0",
              text:
                "Je traîne dans ce camping au bord d'une rivière. Le taff de camille est vraiment plus difficile qu'elle ne penser... Elle a trouver un truc à Motueka et attend une réponse. Je regarde pour trouver un boulot sur place.\n\
\
Finalement faux espoir, rien de concluant.",
            },
            "14/01/2020": {
              km: "60",
              text:
                "J'ai presque plus d'eau et le premier point est à plus de 50 km de là mais ca devrait le faire !\n\
\
Je me lance sur un faux plat qui monte très progressivement, et en fait ça va. C'est plutôt tranquille comme route.\n\
\
J'ai aussi presque plus de batterie donc j'essaye d'économiser et ca me fait louper un embranchement. J'ai fait 4 km de trop...Plus qu'a faire demi-tour et à les faire dans l'autre sens.\n\
\
Finalement, 4 km avant d'arriver à destination je me retrouve fasse à une colline qui ne m'inspire pas du tout confiance ! Le soleil tape vraiment fort et j'ai plus d'eau depuis un petit moment... J'ai quelques vertiges avant d'arriver en haut, je fais une pause à l'ombre pour reprendre mes esprits, mais j'y suis arrivé!\n\
\
Le point d'eau est une air de picnic juste à côté un vieille école des années 1900. J'ai mis 15 mins à trouver le robinet d'eau... J'étais vraiment pas serein s'il n'y en avait pas ! \n\
\
Après avoir vu que le prochain camping est à 50km de là, je décide de rester là pour la nuit.\n\
\
Je rentre le vélo dans l'école et j'installe mon matelas. Pas besoin de se soucier de faire sécher la tente demain matin au moins !",
            },
            "15/01/2020": {
              km: "75",
              text:
                "J'ai super bien dormi dans cette école! Je me suis un peu inquiété d'être vu le matin mais bon même si ça a été le cas... Je sais pas ce qu'ils auraient pu faire.\n\
\
Je prends un chemin de gravier toute la matiné... C'est pas forcément le plus agréable pour rouler mais en terme de paysage et de circulation de voiture c'est parfait ! J'ai croisé une seule voiture pendant toute la matiné. Et j'ai traversé une forêts, des champs à perte de vue, j'étais au milieu d'une vallée... Enfin ça permet de profiter du trajet !\n\
\
Mais vers la fin de la matiné je retrouve la genre d'autoroute... et c'était aussi dans un cadre plutôt sympas, à régulièrement traverser des rivières et globalement ça descendait mais évidemment pas de façon régulière et avec de la monté régulièrement, ça m'a tué.\n\
\
J'arrive assez tôt dans un camping à Murchison au bord de la rivière.",
            },
            "16/01/2020": {
              km: "65",
              text:
                "Je traine pas mal le matin pour profiter de l'électricité et d'internet. J'appel du monde et j'organise mes photos.\n\
\
Après le départ les premiers km s'enchainent bien, c'est plat! Je fais une petite pause au bout de 25 km où je me lance un épisode d'une série... Les cliffhangers se sont enchaîné... Je suis reparti 2h plus tard ^^.\n\
\
J'arrive vers 17h au berlin cafe, un super camping au bord de la rivière!\n\
\
Ca y est les sandflies sont vraiment omniprésentes, heureusement que je suis dans une moustiquaire !",
            },
            "17/01/2020": {
              km: "35",
              text:
                "Au réveil je remercie encore plus ma moustiquaire... Il y a 1 million de sandflies qui grouillent autour. Et aujourd'hui contrairement au soir précédent il y a beaucoup d'humidité. J'attend que le soleil arrive et il se fait attendre... Je pars vers 11h après un petit dej.\n\
\
J'arrive à Westport assez tôt. Je m'attendais à ce que ce soit une ville comme il y a un aéroport mais en fait c'est un patelin de pas plus de 5000 habitants ^^.\n\
\
Mon auberge c'est en fait la maison d'un gars qui a aménagé toutes les pièces qu'il n'utilisait pas pour en faire une auberge et on y est plutôt bien honnêtement.",
            },
            "18/01/2020": {
              km: "10",
              text:
                "Petite grasse matiné. Je pars ensuite à vélo faire le tour des environs. Je tombe sur un trail qui va vers la mer. Il était aménagé pour que ce soit très ludique avec des virages très serrés. Je me suis bien éclaté et ça m'a encore donné envie de faire du VTT! =)\n\
\
Encore des plages gigantesque dont on a vraiment pas l'habitude sur la côte Méditerranéenne.\n\
\
Je retourne au centre ville faire 3 courses puis je rentre à l'auberge organiser la suite de mon périple.",
            },
          },
        },
        "10-south-west-coast": {
          pre: {
            title: "A la rencontre des cyclistes de Nouvelle-Zélande",
            path: "/travels/new-zealand/south-second",
            name: "nz10",
            "date-inter": "18/12/2019 - 06/01/2020",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "nz9",
            "next-link": "nz11",
          },
          data: {
            "19/01/2020": {
              km: "50",
              text:
                "J'ai traîné à avoir sommeil hier soir, ce qui me fait me réveiller assez tard. Le temps de tout ranger et de me faire un petit dej, il est déjà 10h30.\n\
Je pars vers le sud et me rend compte 5 km plus loin que mon gps me faisait aller dans l'autre sens... En vérifiant un peu y'a une solution et elle me semble même meilleur !\n\
J'arrive au cap foulwind auquel on ne peut accéder qu'à pied... Après avoir hésité 5 mins je me décide à pousser le vélo sur les 3,5 km. C'était super pendant un moment mais maintenant je suis face à un portaille... Obligé de porter le vélo pour le passer au dessus... impossible avec les bagages, obligé de tout décharger... Et j'ai du refaire ça 2 autres fois... Quelle galère... Mais ca valait vraiment le coup pour voir ces superbes paysages maritime, un phare, et quelques récit historique! Arrivé vers la fin on a une superbe vue sur une colonie d'otarie ! J'ai pu prendre des photos avec mes jumelles =). Un peu plus loin une île est en fait un site de reproduction d'oiseau, on peut même y voir des pingouins mais ils sont apparement très bien cachés.\n\
Je repars vers le sud en empruntant un route assez jolie ou je fais une course avec des vaches ^^. Elles avaient peur de moi et courraient donc toutes dans le sens ou j'allais...\n\
Je passe sur un pont et je vois un accès à la rivière, un endroit parfait pour manger !\n\
Une ou deux heure à rouler plus tard je passe proche de la mer et je me dis que ce serait cool de trouver un endroit sympas où s'arrêter, je bifurque sur un sentier et je trouve un terrain avec les fondations d'une ancienne baraque qui a brûlé.\n\
Je galère un peu à poser ma tente mais le cadre est vraiment top !",
            },
            "20/01/2020": {
              km: "36",
              text:
                "Je me lève tôt mais je profite de la mer avant de partir vers 11h... très beau cadre, je longe le bord de mer et de temps en temps la route s'enfonce un peu dans les terres pour un paysage plus montagneux. Et comme d'hab c'est jamais plat !\n\
Petite pause sur les rochers pour le midi. Je continue un petit peu et j'arrive dans le village très touristique de Punakaiki. À la sortie du village je vois \"Punakaiki cave\"... Évidemment j'y rentre! Ce n'est pas du tout aménagé et complètement laissé au naturel, j'essaye d'explorer un peu mais à la lumière du téléphone, seul et avec le sol qui commençait à glisser, je choisi d'abandonner. Quelques centaines de mètres plus loin j'atteins le lieu très touristique : \"Pancake rock\", des cailloux avec des couches très régulière. après en avoir fait le tour en vitesse, j'ai envie de me poser malgré la courte journée. Je repère un camping pas loin.\n\
C'est sur le terrain de gens qui ont aménagé une parcelle pour accueillir des campeurs. Y'a juste des toilettes et de l'eau potable.",
            },
            "21/01/2020": {
              km: "45",
              text:
                "En ce moment j'ai vraiment du mal à me motiver à partir tôt le matin, j'attends souvent que la tente ait complètement séché, donc pour changer je pars vers 11h !\n\
La route est superbe! J'enchaine les kilomètres assez vite jusqu'à Greymouth ou je me fais un petit fast food pour midi. Et en reprenant des news de Camille, y'a un petit chamboulement des les plans que j'avais prévu. Du coup je prends l'aprem pour me poser dans un camping pas loin et choisir ce que je fais pour la suite.",
            },
            "22/01/2020": {
              km: "60",
              text:
                "Ce matin je suis motivé! Je suis près pour 8h30 mais finalement je discute avec un gars du camping pendant une bonne demi heure et je prends le temps pour un petit dej... Même si un peu foiré(je sais pas si quelqu'un saurait m'expliquer pourquoi des abricots dans du lait ca donne une genre de brousse), original mais pas mauvais =).\n\
Y'a un trail qui fait toute la côte mais pour longer la route, je préfère être sur du bitume! J'arrive à Hokitika vers midi, je fais 3 courses et je me fais mon déjeuner dans un parc. La ville a l'air bonne à vivre même si hyper touristique une fois de plus! Je passe vers la plage et je me pose 1 petite heure avant de repartir.\n\
Je continue cette fois en empruntant le trail et je l'ai vraiment pas regretté! Un petit chemin au milieu d'une jungle, c'est l'effet que ça donnait. La montée s'étalait sur plusieurs km et était très agréable... Que du bonheur jusqu'à la toute fin de la descente où, il fallait bien que ça arrive un jour, j'ai crevé.\n\
Heureusement j'ai tous les outils mais j'ai jamais fait ça ou en tout cas j'en ai aucun souvenir... Et je me suis pas du tout renseigné avant d'être au milieu d'une forêt sans réseau ^^.\n\
Je m'en sors pour déchausser le pneu en galérant un peu, y'a 2 trous mais assez proche, je me dis qu'en mettant 1 patch pour les 2 ca devrait le fait... Ca marche pas. Ne sachant pas vraiment comment ca marche, j'arrête le dernier cycliste passant par la de la journée(en tout cas c'est l'impression que ça donnait), il est parti à 3h du matin pour faire les 130 km de trail dans les deux sens...respect. Il m'explique qu'il faut que je mette la colle sur la chambre à air que je laisse sécher avant de mettre le patch et qu'il vaut mieux que je coupe celui que j'ai en deux...Ca marche de suite mieux quand on fait les choses bien !\n\
Je repars mais y se fait tard, objectif trouver un coin ou dormir pas loin ! En continuant le trail, je repère un sentier, je vais jusqu'au bout et j'arrive dans un maraie...Rien de concluant...Je continue et je repère que l'herbe a été emprunté récemment sur le côté. Je vais faire un tour et je me dis que ce sera parfait !\n\
Je suis vraiment dans la jungle ! Y fait vraiment hyper humide, et y'a un nombre de moustique vraiment impressionnant... Des vrai moustiques, pas des sandflies, qui elles ne sont pas dans le coin d'ailleurs.",
            },
            "23/01/2020": {
              km: "50",
              text:
                "J'ai déjà était impressionné par les moustique la veille mais ce matin... Je me réveil tôt pour aller au petit coin, je prends ma frontale, j'allume et j'ouvre la tente en même temps... Une image de film d'horreur.... 1 million de moustiques s'engouffrent dans la tente... Je me précipite dehors et je referme vite... En rentrant j'ai passé 20 mins à tuer des moustiques et il en restait encore des tonnes... Abandon et je me rendors un peu.\n\
La pluie me réveil vers 7h... La journée commence mal... Et y'a toujours des milliards de moustiques prêts à rentrer dans la tente... Du coup je patiente mais jusqu'à 10h rien de nouveau, la tente est toujours trempé et les moustiques toujours là .Je me lance dans cette quête de tout ranger et de partir ! Les moustiques m'agressent sans cesse (ce petit sons de moustique j'en pouvais vraiment plus).\n\
Et en sortant le vélo chargé de ma cachette je remarque que les patchs n'ont pas tenu, le pneu est à plat. Je choisi cette fois de changer la chambre à air. Maintenant que je sais comment marche les patchs, je devrais mieux m'en sortir en cas de pepin :). La nouvelle chambre à air n'est pas hyper bien adapté à la roue... Génial... Ça passe quand même en galérant beaucoup mais c'est reparti ! Il est presque 13h...\n\
Et la route est vraiment pas ouf aujourd'hui, pas un moment plat... Et dans un forêt opaque la majeur partie du temps.\n\
Vers 17h je m'arrête dans un camping au bord d'un lac. Très beau panorama mais hier c'etait des moustiques aujourd'hui, des sandflies... Et honnêtement je préfère tellement plus les moustiques ! J'installe vite la tente pour m'y abriter et pour découvrir que j'ai emmener des moustiques avec moi, heureusement très peu.\n\
J'ai acheté des serpentins(truc à allumer et qui fait de la fumée pour éloigner les moustiques)... Ça ne sert strictement à rien contre les sandflies...",
            },
            "24/01/2020": {
              km: "77",
              text:
                "Réveil sous la pluit et sans motivation du tout ! J’aimerais que la pluie s’arrête mais ça n’a pas l’air d’être dans les plans, le soleil ne se lève pas... Je me lance entre deux averses, range la tente mouillé et prépare mes affaires pour partir dans la foulé.\n\
Les km defilent doucement mais sûrement, le dénivelé n'est pas insuportable et je suis motivé par la douche chaude qui m'attend, je dors en auberge ce soir!\n\
Je passe les quelques derniers kilomètres au téléphone avec Camille et enfin j'arrive à destination ! Franz Josef, connu pour son glacier.\n\
L'auberge que j'avais repéré est pleine mais ils m'en conseillent une autre ou il reste de la place :).\n\
En plus dans celle là y'a soupe et petit dej inclu =).\n\
En allant me faire mon diner, je retrouve dans la cuisine des gens avec qui j'avais sympathisés à Auckland ! Ils ont prévu de faire la grosse rando vers le glacier demain et me proposent de les accompagner =).\n\
La soupe est vachement bonne !",
            },
            "25/01/2020": {
              km: "15",
              text:
                "On profite du petit dej en vitesse qui est vraiment pas mal, avec pancakes, gaufres, toasts, céréales et un peu tout ce qui peut faire un bon petit déjeuner. On fait les 5 km qui nous séparent du départ en voiture et c'est parti pour 5h30 de rando!\n\
Très vertical et sur de la roche assez glissante, parfait si on aime ce genre de sensations =). On a traversé des ponts suspendu et pris des escaliers au bord d'une falaise. (Ca m'a donné envie de faire de la Via-ferrata... Y'en a une privatisé pas loin... 300€ pour la faire...)\n\
Arrivé en haut, on a une super vue sur le glacier ! Même si le temps n'est pas très dégagé, on peut quand même le voir monter jusqu'à se confondre avec la neige tout autour, près des sommets.\n\
On a fait la rando difficile mais y'a aussi un chemin facile qui passe par la fond de la vallée et les gens avec qui j'étais l'ont fait la dernière fois et ils m'ont dit qu'il y avait des panneaux indiquant jusqu'où aller le glacier en fonction des années... C'est incroyable à quel point il a reculé... Quand il a été découvert un peu avant 1850 il allait jusqu'à la mer... Aujourd'hui il est 300m au dessus et 10 km plus loins...\n\
On retourne à l'auberge autour de 14h, eux continue directement vers le sud et moi j'ai choisi de faire une nuit de plus ici. J'organise la suite et j'aimerai faire plus d'auberge et moins de camping. L'atmosphère est trop humide, c'est vraiment pas agréable de dormir dans ces conditions. Mais c'est compliqué dans les environs.\n\
Ce soir j'ai vraiment une perte de motivation et j'hésite vraiment à prendre le bus directement pour Queenstown mais il est pas donné et finalement je me décide à avancer à vélo!",
            },
            "26/01/2020": {
              km: "25",
              text:
                "Je voulais partir tôt mais finalement je profite du petit dej, et j'avais pas mal de chose à ranger et organiser.\n\
En sortant je dois faire 3 courses et on devait s'appeler sur la route avec Camille mais je soupçonne fortement qu'il n'y aura pas de réseau plus loin donc je traine un peu en ville.\n\
Juste avant de partir je croise un couple de Francais sur un tandem semi-couché(un vélo tandem avec une personne assis et une couché à l'avant). On parle un peu et finalement on va se faire notre déjeuné ensemble.\n\
Ils sont parti de France y'a 1 an, ont fait toute l'europe depuis la Grèce jusqu'en finlande, puis ils ont pris le transsibérien pour faire l'Asie de l'Est(Chine, Thaïlande, Laos, Tibet...) et pour finalement prendre un avion vers la Nouvelle-Zélande depuis Bangkok ! Et pendant tout ce trajet ils n'ont pas dépensé 1 seul euro en logement, uniquement du camping sauvage et couchsurfing ! Impressionnant!\n\
Ils vont faire la rando et moi je pars vers le sud. Il pleut pas mal depuis le milieu de notre repas, mais ca fait du bien pour rouler ! Il s'arrête de pleuvoir juste quand j'arrive vers une montée... Y faisait tellement chaud !\n\
Et j'en ai bouffé du dénivelé en vraiment peu de kilomètre.... 700m dans les deux sens en 20km... Un bon 6,7% de moyenne... C'était pas facile !\n\
Après avoir parlé avec des gens aussi chaud je pouvais pas prendre une auberge donc je me suis trouvé un petit coin pour camper =). Y'avait même du soleil en fin de journée!",
            },
            "27/01/2020": {
              text:
                "Le jour le moins intéressant du monde ! Il a plu toute la matiné, j'ai voulu partir vers 11h au moment d'une acalmi... J'ai abandonné après avoir enlevé la première sardine puisque la pluie recommençait de plus belle.\n\
Vers 16h ça s'est calmé mais j'avais déjà abandonné l'idée de partir.",
            },
            "28/01/2020": {
              km: "100",
              text:
                "Ce matin je m'étais dit \"départ 6h !\". Bon finalement je me motive vers 7h30 et départ 8h30 mais c'est déjà bien ! Je voulais faire un aller/retour vers la village 2km avant, pour récupérer de l'eau mais je me dis qu'il m'en reste assez.\n\
Le temps est vraiment dégagé aujourd'hui ! Y'a une très belle vue sur les montagnes !\n\
Une demi heure après mon départ je croise deux Allemands à vélo, deux frères qui sont sur les routes depuis 5 ans... Y ont déjà fait depuis l'Allemagne vers le nord de l'Europe puis Island, l'Alaska, descendu toutes l'Amérique jusqu'à Rio puis remonté depuis l'Espagne jusqu'à chez eux y'a 3 ans et maintenant en partant encore une fois de l'Allemagne il ont fait l'Europe de l'est, l'Asie... Et ils sont venu jusqu'ici avec pour objectif les JO de Tokyo... Moi avec mes petits 3000km je fais pâle figure... ^^.\n\
20 mins après en m'arrêtant pour mettre de la musique je vois un cycliste me rattraper. J'abandonne la musique pour l'attendre et roule un peu avec lui.\n\
Un polonais qui fait aussi un trip incroyable, Amérique, puis ici, puis Asie, et lui à un vélo tellement léger comparé à tous les vélos que j'ai pu croiser. Son vélo n'a aucun porte bagage, tout est accroché au guidon, à la selle ou sur l'armature du vélo. Et la dessus il a même de quoi camper !\n\
On avance assez tranquillement et on discute beaucoup sur les 20 premiers km, on fait une petite pause dans un café au bord de mer prendre un petit truc à manger puis on repart en accélérant un peu le rythme... Et il va vite, j'arrive à suivre en forçant un peu mais je tiendrai pas longtemps comme ça. En croisant un autre resto il veut s'arrêter manger, j'en profite pour faire mon petit casse croûte et on repart après ça. Il veut atteindre Haast à 60 km de là et il est 14h et on a déjà fait 60 km ! Je suis motivé pour aller jusqu'au bout même si je suis pas sûr de tenir.\n\
Et effectivement impossible de tenir la cadence, j'ai vraiment tout donner mais j'arrivais pas à le suivre et je voulais vraiment pas le ralentir. Après avoir encore un peu discuté on se dit aurevoir et je m'arrête faire une pause.\n\
Je regarde où j'en suis par rapport à Haast et je me rends compte qu'on a fait 15km en moins de 30mins...Alors que c'était pas vraiment plat hein ! Et il n'était même pas essoufflé ^^ !\n\
Après m'être remis de cette expérience très enrichissante, je repars pour le gros dénivelé de la journée. J'y vais doucement y'a pas de soucis... Jusqu'à un certain moment où je me rends compte que j'ai grave mal aux fesses... Je suis de ouf irrité, je vous montrerai pas de photo mais c'est bien bien rouge... Et honnêtement c'est pas la première fois qu'une chose de ce genre m'arrête, avant c'était entre les jambes parce que je portais un caleçon sous le cuissard... Je sais pas ce qui a provoqué ça mais ça m'oblige à m'arrêter plus tôt que prévu alors qu'il me restait 20km de plat jusqu'à Haast.\n\
J'arrive enfin dans la dernière descente quand je vois un chemin en terre sur le bord de la route, je vais y faire un tour, voir si je peux m'y arrêter. L'endroit n'est vraiment pas accueillant mais ça fera l'affaire.",
            },
            "29/01/2020": {
              km: "25",
              text:
                "Il a plu toute la nuit et j'avais laissé les sacoches sur le vélo... Et c'est vraiment devenu un marais autour de moi.\n\
Je sors tôt pour partir tôt mais la pluie recommence de plus belle, j'abandonne et attend que ça passe.\n\
Je pars vers 13h et même s'il n'y a pas beaucoup à faire... Ne pas pouvoir trop poser les fesses sur la selle ne facilite pas le trajet ! Mais j'arrive 1h plus tard à Haast qui est un coin totalement pommé avec 3 baraques. Je voulais dormir en dortoir mais finalement c'est plein... J'ai pas pu réserver, y'avait pas du tout de réseau les derniers 150 km. Obligé d'aller en camping, ce qui en soit ne me dérange pas mais le moins cher du coin coûte quand même très cher et payer le prix d'un bon dortoir pour un carré d'herbe ne m'enchante jamais beaucoup.\n\
Je m'installe au camping à côté d'un Américain à vélo. Lui se trimballe une remorque derrière le vélo pour ne rien avoir dessus... Je sais pas si c'est vraiment le plus efficace mais ça lui convient, il aime bien pouvoir transporter tout ce qui lui plaît.\n\
J'ai vraiment l'intégralité de mes sacoches trempés... Heureusement il fait beau, ça va sécher rapidement :).\n\
Un peu plus tard dans l'aprem je discute avec un deuxième cycliste, un Français. On décide d'aller se faire un resto juste à côté. Lui a commencé par faire le Te Araroa(la grande randonnée du nord au sud) puis a changé pour finalement partir à vélo suite à un ras le bole du manque de confort. Même si c'est un poil mieux en vélo, c'est toujours pas ça mais au moins ca va plus vite ! D'ailleurs je pensais que la Nouvelle-Zélande c'était pas dangereux mais pendant une rando il s'est fait piqué par une araignée ce qui l'a empêché de continuer 1 mois :/. \n\
En rentrant le ciel est bien dégagé et on a un beau ciel étoilé. C'est le premier que je vois mieux qu'en France ici, mais je reste assez déçu, je m'attendais à mieux.",
            },
            "30/01/2020": {
              km: "150",
              text:
                "Réveil très humide et assez tôt! Je dois tout ranger puisque j'avais vidé toutes mes sacoches hier. Je pars assez tôt histoire de me faire un petit dej avant de prendre le bus :). Ouè, je triche, et je vais encore triché puisqu'une des raisons de ce trajet en bus c'est un trajet en avion pour retourner à Wellington le 4 février.\n\
Arrivé au...Village...Plutôt à ce qu'on pourrait apparenté à une air d'autoroute habité ^^... et donc ça c'est la plus grand agglomération de la région, indiqué sur les panneaux à plus de 300 km de là... je vais dans le petit magasin m'acheter 3 gâteaux avant d'attendre le bus.\n\
Le bus arrive et c'est partie pour 3h, 150km et 2000m de dénivelé.\n\
Honnêtement c'était un trajet vraiment magnifique, c'est vachement dommage de pas l'avoir fait en vélo ! Mais je regrette pas, mon fessier n'aurait pas pu supporter de rouler dès aujourd'hui et finalement la route que j'ai pu faire en suite rivalisait en therme de paysage!\n\
Après avoir quitter le village de Haast, on longe la rivière Haast pour finalement atteindre le cole de Haast... Le conducteur du bus qui nous fait un peu guide touristique nous raconte en même temps l'historique de la zone et c'est vachement intéressant! Et le conducteur a vraiment l'air passionné et il nous le transmet :).\n\
On change ensuite de conducteur et je comprenais plus rien ^^. Un Indien qui ne faisait aucun effort d'accent.\n\
Mais on arrive vers les lacs Wanaka et Hawae qui font chacun plus de 20km de long, c'est vraiment un paysage que j'aime beaucoup !\n\
On arrive ensuite à Wanaka où j'ai réservé une auberge. J'adore la ville ! Très touristique mais quand même assez aéré, des petits bar et snack un peu partout et surtout entouré de montagnes et du lac Wanaka. Je me balade et je profite une partie de l'aprem jusqu'à ce que le vent se lève et que j'opte pour le retour à l'auberge.",
            },
            "31/01/2020": {
              km: "15",
              text:
                "Je prends mon temps, pour profiter du petit dej compris et je sors de l'auberge à 10h. Toujours incapable de poser le cul sur la selle pendant trop longtemps je décide de rester une journée de plus à Wanaka. Je visite les bords du lac toute la mâtiné jusqu'à ce que le vent se lève comme la veille et que ca deviennent la tempête! Du coup je vais dans un snack qui fait des crêpes tenu par des Français et qui semble être le point de regroupement des Francais du coin ^^.\n\
Ensuite je me lance dans une expédition pour trouver un coin ou me caler, je fais quelques km au bord du lac dans un sens puis dans l'autre mais c'est beaucoup trop habité...Finalement j'opte pour un camping payant... C'est vraiment incroyablement cher dans la région!\n\
J'y rencontre un couple de cyclistes qui ont fait la côte Est et qui vont faire une pause pour marcher dans la région et également deux femmes hollandaises qui doivent avoir la 50aine voir 60 ans qui ont fait le même trajet que moi, j'espère que j'aurai l'énergie de faire ça à la retraite ! (Enfin si j'en ai une ;) )\n\
Je traîne le reste de la journée sans rien faire de très intéressant.",
            },
          },
        },
        "11-back-to-wellington": {
          pre: {
            title: "Retour à Wellington",
            path: "/travels/new-zealand/wellington-second",
            name: "nz11",
            "date-inter": "18/12/2019 - 06/01/2020",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "nz10",
            "next-link": "nz12",
          },
          data: {
            "01/02/2020": {
              km: "60",
              text:
                "C'est reparti, les fesses vont mieux et la motivation est à bloc ! En plus tout est secs et il fait super bon !\n\
Je pars donc dans une vallée tout droit jusqu'à Queenstown. Les premiers km sont assez plat puis ça commence à monter tranquillement mais rien de très violent au début et ça finit par trois bon km à grimper ! J'ai pensé m'arrêter pas mal de fois et puis je continuais, je suis assez content de la performance, même si c'est pas grand chose réellement. Une fois arrivé en haut ça valait le coup. Une super vue sur toute la vallée jusqu'à Queenstown !\n\
J'y retrouve les deux Hollandaise qui repartent juste après mon arrivé.\n\
Je repars après m'être couvert un peu parce que le vent souffle fort ici ! Et ca s'arrête pas en descendant... C'était super sympas mais en même temps assez dangereux avec le vent qui m'envoyait dans tous les sens. Ca continue à descendre jusqu'à rejoindre une jonction avec un trail que je ne connaissais pas mais où je vois les deux Hollandaise s'engager.\n\
Super cool le trail, il rejoint le bord d'une rivière puis la suis jusqu'à Queenstown. Je réserve deux nuit en auberge pour demain et après demain mais ce soir j'espère me trouver un coin tranquil (tout est assez cher ici).\n\
Et finalement je me trouve un super coin au bord de la rivière! L'endroit est assez fréquenté mais je pense que je serai pas dérangé pendant la nuit 😊 !\n\
Ah et au fait, j'ai oublié d'écrire ça!!! Plus de sandflies depuis Wanaka ! Quel bonheur ! En installant la tente je me fais une réflexion, la plupart des coins ou je me suis callé en camping sauvage qui est totalement interdit en NZ, j'étais à chaque fois très proche du sentier/route... Je suis vraiment dans la provoque ^^.",
            },
            "02/02/2020": {
              km: "25",
              text:
                "Comme c'était annoncé, il pleut. J'hésite plusieur fois à partir tôt et finalement comme je ne peux check-in dans mon auberge qu'a partir de 14h, je décide d'attendre. L'heure tourne, et à 14h toujours pas d'amélioration pour la pluie. Je sors et à tout ranger mouillé...\n`\
Mais j'ai de la chance, après 1/2h à rouler la pluie s'arrête en majeur partie. J'arrive dans une zone industriel, je fais quelques courses, je n'avais plus rien à manger sans cuisiner.\n\
En me posant pour manger je discute une petite demi heure avec un couple de français venu faire de l'escalade ici. Ils me disent qu'ici le marché de l'occasion est vraiment pas mal et que je devrait voir pour revendre mon matos d'escalade et peut être mon vélo puisque je le retrouverai pour moins cher en France.\n\
Je finis le trajet en suivant un sentier au bord du lac. C'est vraiment hyper stylé comme coin !\n\
Arrivé à l'auberge, c'est pas mal de bordel, ça me fait penser à un des dortoirs dans lequel on a été à Auckland. Je me dis donc que ça doit être un dortoir pour les gens qui restent plusieurs semaines... Et finalement en demandant, pas du tout ^^. C'est juste les gens qui mettent leurs affaires en vrac au milieu de la pièce et qui s'en foutent... Pas si inhabituel que ça réellement. \n\
Après une petite douche je me dirige vers un magasin de vélo pour récupérer un carton pour le vélo. Il m'en file un pour quelques dollars, je pensais que ça allait être plus difficile à trouver ! Je me balade encore un peu, demain c'est pluie toute la journée, j'aurai pas l'occasion de profiter de la ville.",
            },
            "03/02/2020": {
              text:
                "Comme prévu, il pleut toute la journée... J'ai du attendre 17h avant que ça ne se calme. J'ai besoin de scotch, de démonter les pédales de mon vélo et de le laver si possible. Et j'ai de la chance quand je vais au magasin de vélo pour qu'ils me démontent les pédales, ils sont en train de laver un vélo ! Je repars les pédales à la main avec un vélo bien plus présentable 🙂.\n\
Je range tout dans le carton assez facilement je mets moins d'une heure alors que je pensais y passer la soirée.\n\
Demain réveil 6h30... J'utilise un système de navette pour aller à l'aéroport et même en prenant la navette la plus tard il me fait arriver 3h avant le décollage... Tant pis.",
            },
            "04/02/2020": {
              km: "650",
              text:
                "Pratique ce système de navette ! \n\
J'arrive à l'aeroport avec mon gros carton et je dois attendre 1h pour check-in 🙂.Finalement le carton fait 2kg de trop, obligé de l'ouvrir pour retirer un truc. Mais ca se fait et c'est tout bon. Je décolle pour Wellington !\n\
L'atterrissage est très mouvementé, y'a beaucoup de vent ! Comme on pourrait s'y attendre de la ville la plus venteuse de l'hémisphère sud ^^.\n\
Je récupère mon carton éventré.... Heureusement rien ne manque. Je pense qu'ils l'ont ouvert... Je remonte tout ca dans l'aéroport avant de partir vers le centre ville !\n\
Malheureusement Camille travail cet aprèm, elle avait demandé à travailler le matin mais y'a vraiment des soucis à son taff, celui là aussi se passe mal.\n\
Je décide de passer par le bord de mer... Pire idée, j'ai jamais connu un vent pareil... J'ai dû pousser le vélo tout du long... Et des fois je reculais tellement le vent était fort ! Mais finalement je passe le cape et c'est plus calme. Me voilà arriver à l'auberge.\n\
C'est la fin de cette aventure, on va se poser 3 mois ici avant de repartir pour Auckland!",
            },
            "05/02/2020 - 13/05/2020": {
              text:
                "Camille nous a trouvé une coloc ! C'est vraiment trop bien d'avoir notre petit chez nous ! On peut cuisiner tout ce qu'on veut, ça fait du bien après presque 5 mois en camping ou auberge.\n\
\
Après 2 semaines tranquil j'ai cherché un boulot et j'ai trouvé assez rapidement un truc dans la construction. Beaucoup trop traité comme un moins que rien et rien d'intéressant à faire... J'ai décidé de changer de plan après 10 jours de travail de 7h à 17h sans jours de pause. Ici aucunes réglementation concernant les heures supplémentaires, le travail le dimanche, donc peu payé à faire un taff vraiment pourri et à travailler 60 heures par semaines... très peu pour moi.\n\
\
Je suis remonté sur Auckland pour récupérer mon ordi, et je vais essayer de valoriser ce temps à ne pas travailler en faisant un site web 🙂.Quand ca ressemblera à quelque chose je vous montrerais.\n\
Dans la foulé, l'épisode coronavirus a aussi mis la Nouvelle-Zélande en confinement. Le coronavirus est bien mieux gérer ici, en tout cas c'est l'impression qu'on en a. L'ambassade de France incite les Français à rentrer mais après réflexion on va rester ici pour le moment. Malheureusement le confinement s'éternise après la date à laquelle on avait prévu de remonter à Auckland, donc on reste encore quelques temps dans notre coloc à Wellington.\n\
Notre confinement n'a pas été si terrible, on pouvait sortir à notre aise sans s'inquiéter de quelconque restriction comme en France. On a beaucoup cuisiné et fait pas mal de pâtisseries.\n\
Malheureusement je me suis lancé dans une jeu vidéo au début du confinement ce qui m'a un peu dévié du développement de mon site web :(. On a aussi beaucoup cuisiner et prévu notre avenir =).\n\
Ici le gouvernement à choisie la tolérance 0, on est sorti de l'alerte maximum avec une 50aines de cas actifs, et on est finalement sorti du confinement avec 0 cas actif.",
            },
          },
        },
        "12-back-to-auckland": {
          pre: {
            title: "Retour à Auckland",
            path: "/travels/new-zealand/back-to-auckland",
            name: "nz12",
            "date-inter": "14/05/2020 - 07/07/2020",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "nz11",
            "next-link": "nz13",
          },
          data: {
            "14-17/05/2020": {
              km: "1000",
              text:
                "Premier jour du déconfinement! On loue une voiture pour remonter à Auckland dans la journée, Camille commence un stage dans une école primaire dès demain! On est quand même super content d'enfin bouger un peu. \n\
Je pars au petit matin en vélo pour aller jusqu'à l'aéroport récupérer la voiture. Je la récupère un peu en retard pour cause de sous effectif de la compagnie. Le gars était seul pour gérer tout le monde et la voiture devait encore être lavé... Mais ce n'est qu'un petit retard, on est parti pour 700 km! \n\
La route est vraiment stylé tout du long, surtout en passant à côté du Tongariro et de la \"montagne du destin\" du seigneur des anneaux ;)\n\
On repart le lendemain dans l'après-midi d'Auckland après que Camille ait fait une petite réunion pour son stage. On se dirige vers Tauranga pour récupérer son vélo qu'elle avait laissait là 6 mois avant.\n\
Le lendemain on essaye d'aller faire du VTT mais nos vélos ne sont pas vraiment adapté à ça donc on arrête assez vite.\n\
On revient le dimanche à Auckland et on se calle dans l'auberge où on s'est rencontré :)",
            },
            "18-22/05/2020": {
              text:
                "On décide très rapidement de ne pas rester dans cette auberge, la cuisine est beaucoup trop pourri, on ne peut rien y faire de correct... même des pâtes ou des oeufs durs ne sont pas une mince à faire. En plus de ça, Camille à 1h30 de trajet pour aller jusqu'à l'école de son stage.On cherche donc une coloc dans les environs de son école et moi je cherche un boulot près de la zone. J'ai très peu de réponse pour les travails mais on a beaucoup de réponse pour les colocs ! Je la rejoins un soir pour visiter ceux qui nous tente le plus et on choisi de s'installer dès samedi chez un couple de kiwi de notre âge qui ont une maison à 5 mins à pied de l'école de Camille! On va faire le trajet à vélo le samedi!",
            },
            "23/05/2020": {
              km: "35",
              text:
                "Il n'y a pas trop de route normalement donc on prend notre temps avant de partir.\n\
Le départ en plein centre d'Auckland n'a pas été hyper agréable mais on arrive rapidement sur une piste cyclable qu'on avait déjà pris lors de notre premier départ d'Auckland il y a 8 mois! On arrive en début d'après midi après avoir fait le tour de la baie d'Auckland car le pont ne peux pas être traversé à vélo :'(.",
            },
            "24/05/2020 - 06/07/2020": {
              text:
                "Je continue à chercher du taff pendant les premières semaines mais rien ne se concrétise donc je me concentre sur ce blog. \n\
C'est tellement long d'écrire et de mettre en forme tout ça! En plus de tout le texte, il faut séléctionner les photos, les mettre aux bonnes dates et les compresser. Et heureusement qu'avant ça j'ai fait un système qui me permet de ne pas avoir à me soucier de la mise en page, je pense que ça aurait doublé le boulot.\n\
Camille s'est lancé dans un Escape game dans les 3 classes où elle fait son stage. Elle y a passé beaucoup de temps mais à la fin ça a trop bien marché!\n\
On a pris un premier billet d'avion pour Nice via British Airways mais quelques jours après, il a été annulé. Heureusement les lois Européennes nous protège, on a pu facilement obtenir un remboursement. Le seul billet \"accéptable\" qu'on a pu trouver en ligne après ça, allait à Paris. On s'est dit que ce serait une super occasion de traverser la France à vélo ! On part donc le 7 juillet pour Paris en passant par Dubaï.\n\
2 semaines avant le départ une colloc m'a proposé un petit boulot à compter des lunettes pendant quelques jours, ça fait toujours un peu d'argent et c'était plutôt sympas comme job, j'aime bien trier les choses =).",
            },
          },
        },
        conclusion: {
          pre: {
            title: "Conclusion",
            path: "/travels/new-zealand/conclusion",
            name: "nz13",
            "date-inter": "",
            "image-back": "/image/travels/nz/EastCape.jpg",
            "map-back": "/image/travels/nz/MapNZ.svg",
            "back-link": "nz12",
            "next-link": "",
          },
          data: {
            " ": {
              text:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            },
            "Le vélo": {
              text:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            },
          },
        },
      },
    },
  },
};
