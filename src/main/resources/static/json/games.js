export default {
    games: [
        {
            name: "Slay the spire",
            description: "We fused card games and roguelikes together to make the best single player deckbuilder we could."
        },
        {
            name: "Civilization VI",
            description: "Best strategie game"
        },
        {
            name: "Borderlands 2",
            description: "FPS RPG"
        }
    ]
}