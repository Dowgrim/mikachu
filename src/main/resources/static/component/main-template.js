const mainTemplate = `
<div class="height-full">
    <navbar></navbar>
    <router-view></router-view>
</div>
`;

export { mainTemplate };
