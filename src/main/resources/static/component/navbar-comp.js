import { localechanger } from "/component/localechanger-comp.js";

const navbarTemplate = `
<nav id="navBarMain" ref="navBarMain">
    <input id="nav-toggle" ref="nav-toggle" type="checkbox" class="nav-checkbox nav-toggle">
    <div class="animated-icon1 nav-toggle"><span></span><span></span><span></span></div>
    <ul class="nav-wrapper">
        <li><div><router-link class="nav-font" style="font-weight: 700;" to="/">{{ $t("navbar.home") }}</router-link></div></li>
        <li v-if="isDev">
            <div class="dropdown" id="devDropdown">
              <input id="dev-drop" type="checkbox" ref="dropdown" name="dropdown" class="dropdown-toggle hidden-check"></input>
              <label class="nav-font" for="dev-drop">{{ $t("navbar.devs") }} <i class="fa fa-caret-down"></label>
              <div class="dropdown-content" id="menuDev">
                  <router-link class="dropdown-item" to="/developments/sporz" v-if="isSporz">{{ $t("navbar.dev-sporz") }}
                  </router-link>
                  <router-link class="dropdown-item" to="/developments/minesweeper" v-if="isMinesweeper">{{ $t("navbar.dev-dem") }}
                  </router-link>
                  <router-link class="dropdown-item" to="/developments/snake" v-if="isSnake">{{ $t("navbar.dev-sna") }}
                  </router-link>
                  <a class="dropdown-item" href="/developments/deminator" v-if="isDeminator">{{ $t("navbar.dev-sdem") }}
                  </a>
              </div>
            </div>
        </li>
        <li v-if="isTravels">
          <div class="dropdown" id="travelDropdown">
            <input id="travel-drop" type="checkbox" ref="dropdown" name="dropdown" class="dropdown-toggle hidden-check"></input>
            <label class="nav-font" for="travel-drop">{{ $t("navbar.travels") }} <i class="fa fa-caret-down"></label>
            <div class="dropdown-content" id="menu2">
                <a class="dropdown-item" href="/travels/japan" v-if="isJapan">{{ $t("navbar.travel-jap") }}</a>
                <a class="dropdown-item" href="/travels/maroco" v-if="isMoroco">{{ $t("navbar.travel-mar") }}
                </a>
                <a class="dropdown-item" href="/travels/scotland" v-if="isScotland">{{ $t("navbar.travel-sco") }}
                </a>
                <router-link class="dropdown-item" to="/travels/new-zealand" v-if="isNZ">{{ $t("navbar.travel-nz") }}
                </router-link>
            </div>
        </li>
        <li><router-link class="nav-font" to="/about" v-if="isAbout">{{ $t("navbar.about") }}</router-link></li>
        <locale-changer class="locale-changer-div"></locale-changer>
    </ul>
</nav>
`;

const navbar = {
  template: navbarTemplate,
  components: {
    "locale-changer": localechanger,
  },
  methods: {
    navMouseOut: function () {
      var check = this.$refs["nav-toggle"];
      if (check.checked) {
        check.checked = false;
      }
      $(".dropdown-toggle").prop("checked", false);
    },
  },
  mounted() {
    this.$refs["navBarMain"].onmouseleave = this.navMouseOut;
    $(".dropdown-toggle").change(function () {
      $(".dropdown-toggle").not(this).prop("checked", false);
    });
  },
  data() {
    return {
      isDev: true,
      isMinesweeper: true,
      isSnake: true,
      isDeminator: true,
      isSporz: true,
      isTravels: true,
      isMoroco: true,
      isScotland: true,
      isJapan: true,
      isNZ: true,
      isAbout: true,
    };
  },
};

export { navbar };
