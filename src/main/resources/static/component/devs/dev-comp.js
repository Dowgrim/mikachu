const devTemplate = `
<div>
    <link scoped rel="stylesheet" href="/css/dev.css" />
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <h2 class="font-weight-bold text-center"> {{ dev }} </h2>
                <br/><br/>
                <slot></slot>
            </div>
        </div>
    </div>
</div>
`;

const dev = {
  template: devTemplate,
  props: {
    dev: String,
  },
  methods: {},
};

export { dev };
