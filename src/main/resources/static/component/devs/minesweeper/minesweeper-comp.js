import { minesweeperCell } from '/component/dev/minesweeper/minesweeper-cell-comp.js'

const minesweeperTemplate = `
<div class="d-flex flex-row container-mine">
    <div class="mine-settings">
        <div class="default-margin">
            <p class="d-inline">Bomb amount</p>
            <input class="d-inline" v-model="amountOfBombs"> </input>
        </div>
        <div class="d-block default-margin">
            <p class="d-inline">Field size</p>
            <input class="d-inline" v-model="fieldSize" on></input>
        </div>
        <button type="button" class="btn btn-outline-primary default-margin" v-on:click="prepareNewGame">Apply</button>
        <p class="default-margin">Le nombre de bombe est {{ amountOfBombs }} et la taille du champ est {{ fieldSize }} par {{ fieldSize}}</p>
    </div>
    <div class="vertical-line mine-field">
        <div id="minefield" class="align-content-center" oncontextmenu="return false;">
            <div class="row-minesweeper" v-for="row in minefield" :key="row.x">
                <minesweeper-cell v-for="cell in row" :key="cell.y" :cellData="cell" @onLeftClicked="onLeftClicked"
                    @onRightClicked="onRightClicked">
                </minesweeper-cell>
            </div>
        </div>
        <h2 class="text-center" style="margin-top:50px" >{{ textToDisplay }}</h2>
    </div>
</div>
`

const minesweeper = {
    template: minesweeperTemplate,
    components: {
        'minesweeper-cell': minesweeperCell
    },
    data() {
        return {
            firstClickHappened: false,
            gameOver: false,
            amountOfBombs: 10,
            fieldSize: 10,
            bombList: [],
            minefield: [[{
                x: 0,
                y: 0,
                isBomb: false,
                isRevealed: false,
                isMarked: false,
                proximityCount: 0
            }]],
            amountOfcellDiscovered: 0,
            amountOfbombMarked: 0,
            amountOfCellToDiscover: 100,
            textToDisplay: "Good Luck !"
        }
    },
    created() {
        this.prepareNewGame();
    },
    methods: {
        prepareNewGame() {
            this.gameOver = false;
            this.firstClickHappened = false;
            this.minefield.splice(0);
            this.bombList.splice(0);
            this.amountOfcellDiscovered = 0;
            this.amountOfbombMarked = 0;
            this.amountOfCellToDiscover = (this.fieldSize * this.fieldSize) - this.amountOfBombs;

            this.initialiseField();
        },
        initialiseField() {
            for (let x = 0; x < this.fieldSize; x++) {
                this.$set(this.minefield, x, []); for (let y = 0; y < this.fieldSize; y++) {
                    this.$set(this.minefield[x], y, {
                        x: x, y: y, isBomb: false, isRevealed: false, isMarked: false, proximityCount: 0
                    });
                }
            }
        },
        placeMines(clickX, clickY) {
            let exceptsX = [clickX - 1, clickX, clickX + 1];
            let exceptsY = [clickY - 1, clickY, clickY + 1];
            let coords = [];
            for (let x = 0; x < this.fieldSize; x++) {
                for (let y = 0; y < this.fieldSize; y++) {
                    if (!exceptsX.includes(x) || !exceptsY.includes(y)) {
                        coords.push({ x: x, y: y });
                    }
                }
            }

            shuffle(coords);
            let amountOfBombLeftToPlace = this.amountOfBombs;
            while (amountOfBombLeftToPlace > 0 && coords.length > 0) {
                const selectedCoord = coords.pop();
                let bombCell = this.minefield[selectedCoord.x][selectedCoord.y];
                bombCell.isBomb = true;
                this.bombList.push(bombCell);
                this.doForAdjecentCells(selectedCoord.x, selectedCoord.y, function (cell) {
                    cell.proximityCount++;
                });
                amountOfBombLeftToPlace--;
            }
        },
        onLeftClicked(x, y) {
            if (this.gameOver) {
                return;
            }
            if (!this.firstClickHappened) {
                this.firstClickHappened = true;
                this.placeMines(x, y);
            }
            this.discover(this.minefield[x][y]);
        },
        onRightClicked(x, y) {
            if (this.gameOver) {
                return;
            }
            const cell = this.minefield[x][y];
            if (!cell.isRevealed) {
                cell.isMarked = !cell.isMarked;
                if (cell.isBomb && cell.isMarked) {
                    this.amountOfbombMarked++;
                    if (this.amountOfbombMarked == this.amountOfBombs) {
                        this.youWin();
                    }
                }
                if (cell.isBomb && !cell.isMarked) {
                    this.amountOfbombMarked--;
                }
            }
        },
        discover(cell) {
            if (!cell.isRevealed) {
                cell.isRevealed = true;
                this.amountOfcellDiscovered++;
                if (this.amountOfcellDiscovered == this.amountOfCellToDiscover) {
                    this.youWin();
                }
                if (cell.proximityCount == 0) {
                    this.doForAdjecentCells(cell.x, cell.y, this.discover)
                }
                if (cell.isBomb) {
                    this.youLose();
                }
            }
        },
        doForAdjecentCells(x, y, closure) {
            for (let i = -1; i < 2; i++) {
                for (let j = -1; j < 2; j++) {
                    const coordX = x + i; const coordY = y + j;
                    if (coordX < this.fieldSize && coordY < this.fieldSize && coordX >= 0 && coordY >= 0) {
                        closure(this.minefield[coordX][coordY]);
                    }
                }
            }
        },
        youWin() {
            this.textToDisplay = "You Win!!";
        },
        youLose() {
            this.textToDisplay = "You Lose!";
            this.gameOver = true;
        }
    }
}

function shuffle(array) {
    let currentIndex = array.length,
        temporaryValue,
        randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}


export default minesweeper