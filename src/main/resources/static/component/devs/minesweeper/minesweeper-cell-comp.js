const minesweeperCellTemplate = `
<div v-bind:class="{ revealed: isValueVisible}" class="cell" @click="onCellLeftClicked" @click.right="onCellRightClicked" >
        <div class="content" v-show="isValueVisible">{{ value }}</div>
</div>
`

const minesweeperCell = {
    template: minesweeperCellTemplate,
    props: {
        cellData: Object,
        bombIcon: {
            type: String,
            default: "💣"
        },
        flagIcon: {
            type: String,
            default: "❗"
        }
    },
    data() { return {} },
    computed: {
        isValueVisible() {
            console.log("x:" + this.cellData.x + " - y:" + this.cellData.y);
            return this.cellData.isRevealed || this.cellData.isMarked;
        },
        value() {
            if (this.cellData.isRevealed) {
                if (this.cellData.isBomb) {
                    return this.bombIcon;
                } else {
                    return this.cellData.proximityCount == 0 ? "" : this.cellData.proximityCount;
                }
            }
            // Unrevealed is marked or empty
            return this.cellData.isMarked ? this.flagIcon : "";
        }
    },
    methods: {
        onCellLeftClicked() {
            this.$emit('onLeftClicked', this.cellData.x, this.cellData.y);
        },
        onCellRightClicked() {
            this.$emit('onRightClicked', this.cellData.x, this.cellData.y);
        }
    }
}

export { minesweeperCell }