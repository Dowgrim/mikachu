import { dev } from '/component/devs/dev-comp.js';

const sporzTemplate = `
<div class="height-full">
    <dev :dev="'Sporz'">
        <div class="row justify-content-center">
            <div class="col-3 justify-content-center">Nom</div>
            <div class="col-3 justify-content-center">Rôle</div>
            <div class="col-3 justify-content-center">Genome</div>
        </div>
        <div class="player row justify-content-center" v-for="player in players">
            <div class="col-3">{{ player.name }}</div>
            <div class="col-3">{{ player.role }}</div>
            <div class="col-3">{{ player.gen }}</div>
        </div>
        <div>
            <input v-bind:class="{ inputError: wrongName }" class="player" placeholder="Name" v-model="newPlayer" v-on:keydown="keyMonitorPlayer"><button class="btn btn-primary" v-on:click="addPlayer"><i class="fas fa-plus pl-1"></i></button>
        </div>
        <button v-bind:class="{ disabled: !rolesAssigned }" class="btn btn-secondary" v-on:click="launchGame">Lancer la partie</button>
        <button class="btn btn-secondary" v-on:click="assigneRoles">Assigner les rôles</button>
        <button class="btn btn-secondary" v-on:click="testFunction">test Button</button>

        <div>
            <input v-bind:class="{ inputError: wrongName }" class="player" placeholder="Game" v-model="newGame" v-on:keydown="keyMonitorGame"><button class="btn btn-primary" v-on:click="addGame"><i class="fas fa-plus pl-1"></i></button>
        </div>
        <div class="player row justify-content-center" v-for="game in games">
            <div class="col-3">{{ game }}</div>
        </div>
    </dev>
</div>
`

const roles = [
    { role: "Mutant originel", gen: "Hote", muted: true },
    { role: "Médecin", gen: "Normal", muted: false },
    { role: "Médecin", gen: "Normal", muted: false },
    { role: "Psychologue", gen: "Normal", muted: false },
    { role: "Informaticien", gen: "Normal", muted: false },
    { role: "Espion", gen: "Normal", muted: false },
    { role: "Hacker", gen: "Normal", muted: false },
    { role: "L'enquèteur", gen: "Normal", muted: false },
    { role: "Le traitre", gen: "Normal", muted: false },
]

const sporz = {
    template: sporzTemplate,
    components: {
        'dev': dev,
    },
    data() {
        return {
            players: [{ name: "yolo1", role: "Unknown", gen: "Normal", muted: false  },
            { name: "yolo2", role: "Unknown", gen: "Normal", muted: false  },
            { name: "yolo3", role: "Unknown", gen: "Normal", muted: false  },
            { name: "yolo4", role: "Unknown", gen: "Normal", muted: false  },
            ],
            games: [],
            gameStarted: false,
            newPlayer: "",
            newGame: "",
            wrongName: false,
            rolesAssigned: false,
        }
    },
    async mounted() {
        fetch("/developments/sporz/games", {method:'GET'})
        .then((resp) => resp.json()
        .then((res) => res.forEach(element => {
            this.games.push(element);
        })
        ))
        .catch((err)=>console.error(err));
    },
    methods: {
        addPlayer() {
            if (this.newPlayer == "" || this.nameAllreadyTaken(this.newPlayer)) {
                this.wrongName = true;
            }
            else {
                this.players.push({ name: this.newPlayer });
                this.newPlayer = "";
                this.wrongName = false;
            }
        },
        addGame() {
            if (this.newGame == "" || this.nameAllreadyTaken(this.newGame)) {
                this.wrongName = true;
            }
            else {
                fetch("/developments/sporz/newGame", {
                    method:'PUT', 
                    headers: {
                    'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({'name': this.newGame})
                })
                .then((resp) => resp.json()
                .then((res) => {
                    if(res){
                        this.games.push(this.newGame);
                        this.newGame = "";
                    }
                    else{
                        alert("Game allready exist!");
                    }
                }))
                .catch((err)=>console.error(err));
            }
        },
        nameAllreadyTaken(name) {
            for (var i = 0; i < this.players.length; i++) {
                if (name == this.players[i].name) {
                    return true;
                }
            }
            return false;

        },
        playerSetRole(playerInd, roleInd){
            this.players[playerInd].role = roles[roleInd].role;
            this.players[playerInd].gen = roles[roleInd].gen;
            this.players[playerInd].muted = roles[roleInd].muted;
        },
        keyMonitorPlayer(event) {
            if (event.key == "Enter") {
                this.addPlayer();
            }
        },
        keyMonitorGame(event) {
            if (event.key == "Enter") {
                this.addGame();
            }
        },
        assigneRoles() {
            var randNum = 0, tempList = [];
            for (let i = 0; i < this.players.length; i++) {
                tempList.push(i);
            }
            shuffle(tempList);
            for (let i = 0; i < this.players.length; i++) {
                randNum = tempList.pop();
                this.playerSetRole(i, randNum);
            }
            this.rolesAssigned = true;
        },
        launchGame() {
        },
        testFunction() {
            this.players[getRandomInt(this.players.length)].name = "test";
        }

    }
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function shuffle(array) {
    let currentIndex = array.length,
        temporaryValue,
        randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

export default sporz