import { dev } from '/component/devs/dev-comp.js';

const sporzTemplate = `
<div class="height-full">
    <dev :dev="'Sporz'">
        <div>
            <input v-bind:class="{ inputError: wrongName }" class="player" placeholder="Game name" v-model="newGame" v-on:keydown="keyMonitorGame"><button class="btn btn-primary" v-on:click="addGame">New Game</button>
        </div>
        <div class="player row justify-content-center" v-for="game in games">
            <div class="col-3">{{ game }}</div>
        </div>
    </dev>
</div>
`

const roles = [
    { role: "Mutant originel", gen: "Hote", muted: true },
    { role: "Médecin", gen: "Normal", muted: false },
    { role: "Médecin", gen: "Normal", muted: false },
    { role: "Psychologue", gen: "Normal", muted: false },
    { role: "Informaticien", gen: "Normal", muted: false },
    { role: "Espion", gen: "Normal", muted: false },
    { role: "Hacker", gen: "Normal", muted: false },
    { role: "L'enquèteur", gen: "Normal", muted: false },
    { role: "Le traitre", gen: "Normal", muted: false },
]

const sporz = {
    template: sporzTemplate,
    components: {
        'dev': dev,
    },
    data() {
        return {
            games: [],
            gameStarted: false,
            newGame: "",
            wrongName: false,
        }
    },
    async mounted() {
        this.loadGames();
    },
    methods: {
        addGame() {
            if (this.newGame == "") {
                this.wrongName = true;
            }
            else {
                fetch("/developments/sporz/newGame", {
                    method:'PUT', 
                    headers: {
                    'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({'name': this.newGame})
                })
                .then((resp) => resp.json()
                .then((res) => {
                    if(res){
                        this.$router.push({ name: 'sporz-admin', params: { name: this.newGame } });
                    }
                    else{
                        alert("Game allready exist!");
                    }
                }))
                .catch((err)=>console.error(err));
            }
        },
        loadGames(){
            fetch("/developments/sporz/games", {method:'GET'})
            .then((resp) => resp.json()
            .then((res) => res.forEach(element => {
                this.games.push(element);
            })
            ))
            .catch((err)=>console.error(err));
        },
        keyMonitorGame(event) {
            if (event.key == "Enter") {
                this.addGame();
            }
        }
    }
}

export default sporz