
const sporzAdminTemplate = `
<div class="container">
    <div class="justify-content-center">
        {{game}}
        <div v-for="player in players">
            {{ player.name }}
        </div>
        <div>
            <input placeholder="Name" v-model="newPlayer" v-on:keydown="keyMonitor" ><button class="btn btn-primary" v-on:click="addPlayer"><i class="fas fa-plus pl-1"></i></button>
        </div>
    </div>
</div>
`

const sporzAdmin = {
    template: sporzAdminTemplate,
    components: {
    },
    data() {
        return {
            game : "",
            players: [{
                name: "",
                gen: "normal",
                mute: false,
                role: "Astronaute",
            }
            ],
            gameStarted: false,
            gameOver: false,
            newPlayer: "",
            timer: {},
            textToDisplay: "Good Luck !"
        }
    },
    async mounted() {
        this.loadGame(this.$route.params.name);
    },
    methods: {
        addPlayer() {
            this.players.push({ name: this.newPlayer });
            this.newPlayer = "";
        },
        keyMonitor(event) {
            if (event.key == "Enter") {
                this.addPlayer();
            }
        },
        loadGame(name) {
            fetch("/developments/sporz/"+ name, {method:'GET'})
            .then((resp) => resp.json()
            .then((res) => this.game = res.name))
            .catch((err)=>console.error(err));
        }
    }
}


export default sporzAdmin