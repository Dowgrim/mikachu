const snakeCellTemplate = `
<div v-bind:class="{ isSnake: isSnake, isApple: isApple }" class="cell-snake">
        <div class="content" v-show="isValueVisible"></div>
</div>
`

const snakeCell = {
    template: snakeCellTemplate,
    props: {
        cellData: Object,
        bombIcon: {
            type: String,
            default: "💣"
        },
    },
    data() { return {} },
    computed: {
        isValueVisible() {
            return this.cellData.isSnake;
        },
        isSnake() {
            return this.cellData.isSnake;
        },
        isApple() {
            return this.cellData.isApple;
        }
    },
}

export { snakeCell }