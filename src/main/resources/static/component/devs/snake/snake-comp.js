import { snakeCell } from '/component/dev/snake/snake-cell-comp.js'

const KEYNORTH = 'ArrowUp';
const NORTH = { x: -1, y: 0 };
const KEYSOUTH = 'ArrowDown';
const SOUTH = { x: 1, y: 0 };
const KEYEAST = 'ArrowRight';
const EAST = { x: 0, y: 1 };
const KEYWEST = 'ArrowLeft';
const WEST = { x: 0, y: -1 };
const KEYPAUSEPLAY = ' ';

const snakeTemplate = `
<div class="container" v-on:keyup="onKeyPressed">
    <div class="row justify-content-center">
        <div class="mine-settings col-md-3">
            <div class="d-block default-margin">
                <p class="d-inline">Field size</p>
                <input class="d-inline" v-model="fieldSize" on></input>
                <p class="d-inline">Snake speed</p>
                <input class="d-inline" v-model="snakeSpeed" on></input>
            </div>
            <button type="button" class="btn btn-outline-primary default-margin" v-on:click="pauseGame">Apply speed</button>
            <button type="button" class="btn btn-outline-primary default-margin" v-on:click="prepareNewGame">Restart</button>
            <p class="default-margin">La taille du champ est {{ fieldSize }} par {{ fieldSize}}</p>
        </div>
        <div class="col-md-9 align-content-center">
            <div id="field" class="d-inline-block border border-dark">
                <div class="row-snake" v-for="row in field" :key="row.x">
                    <snake-cell v-for="cell in row" :key="cell.y" :cellData="cell" ></snake-cell>
                </div>
            </div>
            <h2 class="text-center" style="margin-top:50px" >{{ textToDisplay }}</h2>

        </div>
    </div>
</div>
`

const snake = {
    template: snakeTemplate,
    components: {
        'snake-cell': snakeCell
    },
    data() {
        return {
            gameStarted: false,
            gameOver: false,
            appleAte: 0,
            snakeDirection: { x: 0, y: 1 },
            snakeSpeed: 5,
            snakeSize: 5,
            fieldSize: 40,
            snake: [],
            field: [[{
                x: 0,
                y: 0,
                isSnake: false,
                isApple: false,
                isWall: false
            }]],
            timer: {},
            textToDisplay: "Good Luck !"
        }
    },
    created() {
        this.prepareNewGame();
        window.addEventListener('keydown', this.onKeyPressed);
    },
    methods: {
        prepareNewGame() {
            this.pauseGame();
            this.gameOver = false;
            this.field.splice(0);
            this.appleAte = 0;

            this.initialiseField();
            this.placeSnake();
        },
        initialiseField() {
            for (let x = 0; x < this.fieldSize; x++) {
                this.$set(this.field, x, []);
                for (let y = 0; y < this.fieldSize; y++) {
                    this.$set(this.field[x], y, { x: x, y: y, isSnake: false, isApple: false, isWall: false });
                }
            }
        },
        placeSnake() {
            var snakeX = Math.floor((Math.random() * (this.fieldSize - 10)) + 5);
            var snakeY = Math.floor((Math.random() * (this.fieldSize - 10)) + 5);

            this.setSnake(snakeX, snakeY);
            var tempSnakeX, tempSnakeY;
            if (snakeX > (this.fieldSize / 2)) {
                tempSnakeX = Math.abs(snakeX - this.fieldSize);
            }
            if (snakeY > (this.fieldSize / 2)) {
                tempSnakeY = Math.abs(snakeY - this.fieldSize);
            }
            if (tempSnakeX < tempSnakeY) {
                if (snakeX < (this.fieldSize / 2)) {
                    this.snakeDirection = SOUTH;
                }
                else {
                    this.snakeDirection = NORTH;
                }
            }
            else {
                if (snakeY < (this.fieldSize / 2)) {
                    this.snakeDirection = EAST;
                }
                else {
                    this.snakeDirection = WEST;
                }
            }
            for (let i = 1; i < this.snakeSize; i++) {
                this.setSnake(snakeX + (i * this.snakeDirection.x), snakeY + (i * this.snakeDirection.y));
            }
        },
        placeApple() {
            var appleX = 0, appleY = 0;
            do {
                var appleX = Math.floor((Math.random() * this.fieldSize));
                var appleY = Math.floor((Math.random() * this.fieldSize));
            } while (this.field[appleX][appleY].isSnake);
            this.field[appleX][appleY].isApple = true;
        },
        eatApple(x, y) {
            this.field[x][y].isApple = false;
            this.appleAte++;
            this.snakeSize++;
            this.placeApple();
        },
        setSnake(x, y) {
            this.field[x][y].isSnake = true;
            if (this.snake.unshift(this.field[x][y]) > this.snakeSize) {
                this.snake.pop().isSnake = false;
            }
        },
        onKeyPressed(event) {
            switch (event.key) {
                case KEYNORTH:
                    this.snakeDirection = { x: -1, y: 0 };
                    break;
                case KEYSOUTH:
                    this.snakeDirection = { x: 1, y: 0 };
                    break;
                case KEYEAST:
                    this.snakeDirection = { x: 0, y: 1 };
                    break;
                case KEYWEST:
                    this.snakeDirection = { x: 0, y: -1 };
                    break;
                case KEYPAUSEPLAY:
                    if (this.gameStarted) {
                        this.pauseGame();
                    }
                    else {
                        this.startGame()
                    }
                    break;
                default:
                    break;
            }
        },
        startGame() {
            this.gameStarted = true;
            this.placeApple();
            this.timer = setInterval(this.moveSnake, (5 / this.snakeSpeed * 100));
        },
        pauseGame() {
            this.gameStarted = false;
            clearInterval(this.timer);
        },
        moveSnake() {
            console.log("snake[0].x + dir.x ===== " + this.snake[0].x + this.snakeDirection.x);
            var x = this.snake[0].x + this.snakeDirection.x;
            var y = this.snake[0].y + this.snakeDirection.y;
            this.setSnake(x, y);
            if (this.field[x][y].isApple) {
                this.eatApple(x, y);
            }
        },
        youWin() {
            this.textToDisplay = "You Win!!";
        },
        youLose() {
            this.textToDisplay = "You Lose!";
            this.gameOver = true;
        }
    }
}


function shuffle(array) {
    let currentIndex = array.length,
        temporaryValue,
        randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}


export default snake