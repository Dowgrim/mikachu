import images from '/json/images.js';

const homeTemplate = `
<div class="height-full">
    <div class="home-background" v-bind:style="{ 'background-image': 'url(' + backgroundFirstImage + ')'}"></div>
    <div class="home-background" v-bind:class="{fade:firstImage, unfade:!firstImage}" v-bind:style="{ 'background-image': 'url(' + backgroundSecondImage + ')'}"></div>
    <div class="home-text">{{textToDisplay}}</div>
</div>
`
const home = {
    template: homeTemplate,
    data() {
        return {
            backgroundFirstImage: "'/image/home/Corse-moto.jpg'",
            backgroundSecondImage: "'/image/home/Corse-moto.jpg'",
            firstImage: true,
            imagesList: [],
            textToDisplay: ""
        }
    },
    created() {
        this.loadImages();
        setInterval(this.updateImage, 5000);
    },
    methods: {
        loadImages() {
            let test = [];
            images.images.forEach(element => {
                this.imagesList.push("'/image/home/" + element.path + "'");
            });
            shuffle(this.imagesList);
            this.backgroundSecondImage = this.imagesList[2];
        },
        updateImage() {
            let img = this.imagesList.shift();
            if (this.firstImage) {
                this.backgroundSecondImage = img;
            } else {
                this.backgroundFirstImage = img;
            }
            this.imagesList.push(img);
            this.firstImage = !this.firstImage;
        }
    }

}

function shuffle(array) {
    let currentIndex = array.length,
        temporaryValue,
        randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

export { home }
