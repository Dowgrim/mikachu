const localechangerTemplate = `
<li class="nav-font" style="margin-left: auto">
  <select class="locale-changer" v-model="$i18n.locale">
      <option v-for="(lang, i) in langs" v-bind:key="i" v-bind:value="lang.lang">
          <i class="france flag"></i>{{ lang.lang }}
      </option>
  </select>
</li>
`;

const localechanger = {
  template: localechangerTemplate,
  name: "locale-changer",
  data() {
    return {
      langs: [
        {
          lang: "en",
          flag: "gb uk",
        },
        {
          lang: "fr",
          flag: "fr",
        },
      ],
    };
  },
};

export { localechanger };
