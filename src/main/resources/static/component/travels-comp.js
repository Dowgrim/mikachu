const travelsTemplate = `
<div>
    <ul>
        <li>Around Europe</li>
        <li>Japan</li>
        <li>Morocco</li>
        <li>Scotland</li>
        <li>New Zealand</li>

    </ul>
</div>
`

const travels = {
    template: travelsTemplate,
}

export { travels }