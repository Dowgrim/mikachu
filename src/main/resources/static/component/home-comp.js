import { footer } from "/component/footer-comp.js";

const homeTemplate = `
<div>
    <div class="home-background" v-bind:style="{ 'background-image': 'url(' + backgroundImage + ')'}"></div>
    <div class="default-margin">
        <div>
            <p>{{ $t("home.welcome") }}</p>
            <p>{{ $t("home.why") }}</p>
            </br>
            <p>{{ $t("home.wish") }}</p>
        </div>
    </div>
</div>
`;
const home = {
  template: homeTemplate,
  components: {},
  data() {
    return {
      backgroundImage: "'/image/home/Corse-moto.jpg'",
      textToDisplay: "",
    };
  },
  created() {},
  methods: {},
};

export { home };
