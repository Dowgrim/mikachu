const ImagePTemplate = `
<div class="position-relative">
    <img class="rounded image-P" v-bind:src="imgPData" :class="{'img-enlarged': isEnlarged}">
    <button class="btn-shrink clear-btn btn-lg" v-on:click="shrink()" v-if="isEnlarged"><i class="fas fas fa-times" v-if></i></button>    
    <button class="btn-enlarge clear-btn btn-lg" v-on:click="enlarge()" v-else><i class="fas fa-expand" v-if></i></button>
</div>
`


const imageP = {
    template: ImagePTemplate,
    props: {
        imgPData: Object,
    },
    data() {
        return {
            isEnlarged: false,
        }
    },
    computed: {

    },
    methods: {
        enlarge() {
            this.isEnlarged = true;
        },
        shrink() {
            this.isEnlarged = false;
        }
    }
}

export { imageP }
