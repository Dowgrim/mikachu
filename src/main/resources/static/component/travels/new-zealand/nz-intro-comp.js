import { travel } from '/component/travels/travel-comp.js'

const nzIntroTemplate = `
<div class="height-full">
    <travel page="nz.intro">
        <p class="text-nz">{{ $t("travels.nz.intro.how") }}</p>
        
        <br/><br/>
        
        <p class="text-nz">{{ $t("travels.nz.intro.todo") }}</p>
        <h2>&#9679  {{ $t("travels.nz.intro.visa-title") }}</h2>
        <p class="text-nz">{{ $t("travels.nz.intro.visa-text") }}</p>
        <h2>&#9679  {{ $t("travels.nz.intro.money-title") }}</h2>
        <p class="text-nz">{{ $t("travels.nz.intro.money-text") }}</p>
        <h2>&#9679  {{ $t("travels.nz.intro.taff-title") }}</h2>
        <p class="text-nz">{{ $t("travels.nz.intro.taff-text") }}</p>
        <br/><br/>
        <p class="text-nz">{{ $t("travels.nz.intro.conclusion") }}</p>
        <br/>
        <p class="text-nz">{{ $t("travels.nz.intro.fb") }}</p>
    </travel>
</div>
`
const nzIntro = {
    template: nzIntroTemplate,
    components: {
        'travel': travel
    },
    data() {
        return {
        }
    }
}

export default nzIntro 
