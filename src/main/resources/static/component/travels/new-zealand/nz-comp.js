import { nzDay } from '/component/travels/new-zealand/nz-day-comp.js';
import { travel } from '/component/travels/travel-comp.js';

const travelNZTemplate = `
<travel :page="page">
    <nz-day v-for="(obj, index) in pageData" :date="index" :page="page"></nz-day>
</travel>
`

const travelNZ = {
    template: travelNZTemplate,
    components: {
        'travel': travel,
        'nz-day': nzDay,
    },
    props: {
        page: String,
    },
    computed: {
        pageData() { return this.$t("travels." + this.page + ".data"); },
    },
}

export { travelNZ }
