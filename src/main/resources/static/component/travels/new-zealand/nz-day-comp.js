import images from '/json/travel-images.js';
import { imageP } from '/component/travels/image-comp.js';

const nzDayTemplate = `
<div>
    <br>
    <span>{{ date }}</span><span v-if="iskm"> - {{ km }}kms</span>
    <p class="text-nz">{{ text }}</p>
    
    <imageP class="d-inline-block" v-for="(imgP, index) in pics" :key="index" :imgPData="imgP" v-if="isPics">
    </imageP>
    <br>
</div>
`


const nzDay = {
    template: nzDayTemplate,
    components: {
        'imageP': imageP,
    },
    props: {
        page: String,
        date: String,
    },
    computed: {
        km() { return this.$t("travels." + this.page + ".data." + this.date + ".km"); },
        text() {
            return this.$t("travels." + this.page + ".data." + this.date + ".text");
        },
        pics() {
            let str = this.page.split(".");
            try {
                return images[str[0]][str[1]][this.date.replace(/\//g, "-")];
            }
            catch (err) {
                return undefined;
            }
        },
        iskm() { return this.km != "travels." + this.page + ".data." + this.date + ".km"; },
        isPics() { return this.pics != undefined; },
    },
}

export { nzDay }
