import { nzDay } from "/component/travels/new-zealand/nz-day-comp.js";

const travelTemplate = `
<div>
    <link rel="stylesheet" href="" />
    <div class="home-background image-back" v-bind:style="{ 'background-image': 'url(' + imageBack + ')'}"></div>
    <div class="home-background map-back" v-bind:style="{ 'background-image': 'url(' + mapBack + ')'}"></div>
    <div>
        <button class="btn-transition btn-left" v-show="isBackPossible" v-on:click="pushBackward"><i class="fas fa-arrow-left pl-1"></i></button>
        <button class="btn-transition btn-right" v-show="isForwardPossible" v-on:click="pushForward"><i class="fas fa-arrow-right pl-1"></i></button>
        <div class="container">
            <h2 class="font-title text-center">{{ title }} </h2>
            <h5 class="text-center">{{ dateInter }}</h5>
            <br/><br/>
            <slot></slot>
        </div>
    </div>
</div>
`;

const travel = {
  template: travelTemplate,
  components: {
  },
  props: {
    page: String,
  },
  mounted() {
    if(!document.querySelector('#travelCSS')){
      let style = document.createElement('link');
      style.id = "travelCSS"
      style.type = "text/css";
      style.rel = "stylesheet";
      style.href = '/css/travel.css';
      document.head.appendChild(style);
    }
    window.scrollTo(0,0);
  },
  computed: {
    imageBack() {
      return this.$t("travels." + this.page + ".pre.image-back");
    },
    mapBack() {
      return this.$t("travels." + this.page + ".pre.map-back");
    },
    nextLink() {
      return this.$t("travels." + this.page + ".pre.next-link");
    },
    backLink() {
      return this.$t("travels." + this.page + ".pre.back-link");
    },
    title() {
      return this.$t("travels." + this.page + ".pre.title");
    },
    dateInter() {
      return this.$t("travels." + this.page + ".pre.date-inter");
    },
    pageData() {
      return this.$t("travels." + page + ".data");
    },
    isBackPossible() {
      return (
        this.backLink != "" &&
        this.backLink != undefined &&
        this.backLink != "travels.nz.home.pre.back-link"
      );
    },
    isForwardPossible() {
      return (
        this.nextLink != "" &&
        this.nextLink != undefined
      );
    },
  },
  methods: {
    pushForward() {
      if (this.isForwardPossible) this.$router.push({ name: this.nextLink });
    },
    pushBackward() {
      if (this.isBackPossible) {
        this.$router.push({ name: this.backLink });
      }
    },
  },
};

export { travel };
