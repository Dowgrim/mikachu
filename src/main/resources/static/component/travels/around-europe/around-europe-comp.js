import { travel } from '/component/travels/travel-comp.js';
const nzHomeTemplate = `
<div class="height-full">
    <travel v-bind:page="'nz.home'">
        <p class="text-nz">{{ $t("travels.nz.home.intro-text") }}</p>
        <div v-for="(page, index) in pagesList" :key="index">
            <router-link class="link-nz" :to="page.path">{{ page.title }} </router-link><br>
        </div>
    </travel>
</div>
`
const nzHome = {
    template: nzHomeTemplate,
    components: {
        'travel': travel,
    },
    data() {
        return {

        }
    },
    computed: {
        pagesList() {
            var pageListReturn = [];
            for (let [key, value] of Object.entries(this.$t("travels.nz"))) {
                if (key != "home") {
                    pageListReturn.push({ "path": value.pre.path, "title": value.pre.title });
                }
            }
            console.log(pageListReturn);
            return pageListReturn;
        },
    }
}

export default nzHome 
