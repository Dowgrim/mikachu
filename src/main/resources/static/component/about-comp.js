import { footer } from '/component/footer-comp.js'

const aboutTemplate = `
<div class="container">
    <link rel="stylesheet" href="/css/about.css" />
    <div class="home-background" id="image1" v-bind:style="{ 'background-image': 'url(' + backgroundImage + ')'}"></div>
    <div class="home-background image-about-2" id="image2" v-bind:style="{ 'background-image': 'url(' + backgroundImage2 + ')'}"></div>
    <div class="home-background image-about-3" id="image3" v-bind:style="{ 'background-image': 'url(' + backgroundImage3 + ')'}"></div>
    <div class="home-background image-about-4" id="image4" v-bind:style="{ 'background-image': 'url(' + backgroundImage4 + ')'}"></div>
    <div class="row">
    <div class="col-4">
        <div id="list-example" class="list-group position-fixed nav-about">
            <a class="list-group-item list-group-item-action active" href="#image1">Item 1</a>
            <a class="list-group-item list-group-item-action" href="#image2">Item2</a>
            <a class="list-group-item list-group-item-action" href="#image3">Item 3</a>
            <a class="list-group-item list-group-item-action" href="#image4">Item 4</a>
        </div>
    </div>
</div>
`
const about = {
    template: aboutTemplate,
    components: {
        'footerP': footer
    },
    created() {
        $(document).ready(function () {
            $("body").scrollspy({
                target: "#list-example",
                offset: 100
            });
        });
    },
    data() {
        return {
            backgroundImage: "/image/home/Corse-moto.jpg",
            backgroundImage2: "/image/home/Dublin.jpg",
            backgroundImage3: "/image/home/IMG_20181228_1319532.jpg",
            backgroundImage4: "/image/home/IMG_20190102_1126126.jpg",
        }
    }

}

export { about }