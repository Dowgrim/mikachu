import Vue from "https://cdn.jsdelivr.net/npm/vue@latest/dist/vue.esm.browser.min.js";

import i18nText from "/json/i18n.js";

import { mainTemplate } from "/component/main-template.js";

import { home } from "/component/home-comp.js";
import { navbar } from "/component/navbar-comp.js";
import { about } from "/component/about-comp.js";

Vue.use(VueRouter);
Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: "fr",
  fallbackLocale: "fr",
  messages: i18nText,
  silentTranslationWarn: true,
});

const allRoutes = [
  {
    path: "/travels/new-zealand",
    component: () => import("/component/travels/new-zealand/nz-home-comp.js"),
    name: "newZealand",
  },
  {
    path: "/travels/new-zealand/introduction",
    component: () => import("/component/travels/new-zealand/nz-intro-comp.js"),
    name: "beforeNZ",
  },
];

import { travelNZ } from "/component/travels/new-zealand/nz-comp.js";
const nzTemplate = `
<div class="height-full">
    <nz-travel v-bind:page="page"></nz-travel>
</div>
`;

for (let [keyParent, valueParent] of Object.entries(i18nText.fr.travels)) {
  for (let [key, value] of Object.entries(valueParent)){
    if (key != "home" && key != "intro") {
      allRoutes.push({
        path: value.pre.path,
        component: {
          template: nzTemplate,
          components: {
            "nz-travel": travelNZ,
          },
          data() {
            return { page: keyParent + "." + key };
          },
        },
        name: value.pre.name,
      });
    }
  }
}
allRoutes.push(
  ...[
    {
      path: "",
      component: home,
      name: "Welcome",
    },
    {
      path: "/about",
      component: about,
      name: "About Page",
      meta: {
        title: "about",
      },
    },
    // Sporz
    {
      path: "/developments/sporz",
      component: () => import("/component/devs/sporz/sporz-comp.js"),
      name: "sporz",
    },
    {
      path: "/developments/sporz/sporz-game/admin/:name",
      component: () => import("/component/devs/sporz/sporz-admin-comp.js"),
      name: "sporz-admin",
    },
    {
      path: "/developments/sporz/sporz-game/:name",
      component: () => import("/component/devs/sporz/sporz-player-comp.js"),
      name: "sporz-player",
    },
    // MineSweeper
    {
      path: "/developments/minesweeper",
      component: () =>
        import("/component/devs/minesweeper/minesweeper-comp.js"),
      name: "minesweeper",
    },
    //Snake
    {
      path: "/developments/snake",
      component: () => import("/component/devs/snake/snake-comp.js"),
      name: "snake",
    },
  ]
);
const router = new VueRouter({
  mode: "history",
  routes: allRoutes,
});

var app = new Vue({
  el: "#contents",
  router,
  i18n,
  components: {
    navbar: navbar,
    about: about,
  },
  template: mainTemplate,
});

export { app };
