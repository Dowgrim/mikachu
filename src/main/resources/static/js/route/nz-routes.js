const nzRoutes = [
    {
        path: '/travels/new-zealand',
        component: () => import('/component/travels/new-zealand/nz-home-comp.js'),
        name: "newZealand"
    },
    {
        path: '/travels/new-zealand/introduction',
        component: () => import('/component/travels/new-zealand/nz-intro-comp.js'),
        name: "beforeNZ"
    },
    {
        path: '/travels/new-zealand/first-month',
        component: () => import('/component/travels/new-zealand/nz-firstmonth-comp.js'),
        name: "nz1"
    },
    {
        path: '/travels/new-zealand/first-week',
        component: () => import('/component/travels/new-zealand/nz-firstweek-comp.js'),
        name: "nz2"
    }
]

export { nzRoutes }