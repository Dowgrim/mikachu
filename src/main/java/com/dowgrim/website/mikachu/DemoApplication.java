package com.dowgrim.website.mikachu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;

@SpringBootApplication
public class DemoApplication {
	static FileWriter file;

	public static void main(String[] args) {
		File folder = new File("./src/main/resources/static/image/travels");
		JSONObject travelsObject = new JSONObject();
		for (final File travel : folder.listFiles()) {
			if (!travel.isDirectory()) {
				continue;
			}
			JSONObject travelObject = new JSONObject();
			System.out.println(travel.getName());
			for (final File page : travel.listFiles()) {
				if (!page.isDirectory()) {

					continue;
				}
				JSONObject pageObject = new JSONObject();
				System.out.println(page.getName());
				for (final File date : page.listFiles()) {
					if (!date.isDirectory()) {
						continue;
					}
					JSONArray dateObject = new JSONArray();
					// System.out.println(date.getName());
					for (final File fileEntry : date.listFiles()) {
						// System.out.println(fileEntry.getName());
						dateObject.put("/image/travels/" + travel.getName() + "/" + page.getName() + "/"
								+ date.getName() + "/" + fileEntry.getName());

					}
					try {
						pageObject.put(date.getName(), dateObject);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				try {
					travelObject.put(page.getName(), pageObject);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				travelsObject.put(travel.getName(), travelObject);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			file = new FileWriter("./src/main/resources/static/json/travel-images.js");
			file.write("export default " + travelsObject.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				file.flush();
				file.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		SpringApplication.run(DemoApplication.class, args);
	}
}