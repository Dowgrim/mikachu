package com.dowgrim.website.mikachu.model;

public class SporzGame {

    private String name;

    public SporzGame() {
    }

    public SporzGame(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{name:" + name + "}";
    }

}