package com.dowgrim.website.mikachu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("/")
    public String index(final Model model) {
        model.addAttribute("eventName", "Un bon jeu");
        return "index.html";
    }

    @GetMapping("/**/{path:[^\\.]*}")
    public String anything(final Model model) {
        model.addAttribute("eventName", "Un bon jeu");
        return "index.html";
    }

}