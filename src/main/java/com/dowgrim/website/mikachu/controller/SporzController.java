package com.dowgrim.website.mikachu.controller;

import java.util.*;

import com.dowgrim.website.mikachu.model.SporzGame;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SporzController {

    private Map<String, SporzGame> games = new HashMap<String, SporzGame>();

    @PutMapping("/developments/sporz/newGame")
    public Boolean newGame(@RequestBody SporzGame newGame) {
        if (games.containsKey(newGame.getName())) {
            return false;
        } else {
            games.put(newGame.getName(), newGame);
        }
        return true;
    }

    @GetMapping("/developments/sporz/{gameName}")
    public SporzGame getGame(@PathVariable(value = "gameName") String gameName) {
        return games.get(gameName);
    }

    @GetMapping("/developments/sporz/games")
    public String[] getGames() {
        return games.keySet().toArray(new String[0]);
    }

}